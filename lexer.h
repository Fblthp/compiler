#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <exception>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <algorithm>
#include <memory>
#include "tokenTypes.h"

struct Token
{
	std::string token;
	TokenType type;
    int line;
    int column;

    Token(std::string token, TokenType type, int line, int column) : token(token), type(type), line(line), column(column) {};
    Token(char token, TokenType type, int line, int column) : token(std::string(1, token)), type(type), line(line), column(column) {};
    Token(std::string token, TokenType type) : token(token), type(type), line(0), column(0) {};
    Token(char token, TokenType type) : token(std::string(1, token)), type(type), line(0), column(0) {};

    //const
    virtual std::string getValueAsString() = 0;
};

struct StringValueToken : public Token
{
    std::string value;
    StringValueToken(std::string token, TokenType type, std::string value) : Token(token, type), value(value) {};
    StringValueToken(std::string token, TokenType type,
                     std::string value, int line, int pos) : Token(token, type, line, pos), value(value) {};
    StringValueToken(char token, TokenType type, std::string value) : Token(token, type), value(value) {};
    virtual std::string getValueAsString(){return value;};
};

struct IntValueToken : public Token
{
	int value;
    IntValueToken(std::string token, TokenType type, int value) : Token(token, type), value(value) {};
    IntValueToken(char token, TokenType type, int value) : Token(token, type), value(value) {};
    virtual std::string getValueAsString()
    {
        std::stringstream ss;
        ss << value;
        return ss.str();
    };
};

struct FloatValueToken : public Token
{
	float value;
    FloatValueToken(std::string token, TokenType type, float value) : Token(token, type), value(value) {};
    FloatValueToken(char token, TokenType type, float value) : Token(token, type), value(value) {};
    virtual std::string getValueAsString()
    {
        std::stringstream ss;
        ss << value;
        return ss.str();
    };
};

typedef std::shared_ptr<Token> token_ptr;

class InvalidToken : public std::exception
{
private:
    std::string message;
public:
	InvalidToken(int lineIdx, int columnIdx, std::string messageText){
        std::stringstream ss;
		ss << "unknown token line " << lineIdx << " pos " << columnIdx << '\n';
        ss << messageText;
		message = ss.str();
	};
	const char* what(){
		return message.c_str();
	}
};

class Lexer
{
private:
    std::ifstream source;
	bool eof;

    std::stringstream currentToken;
	char nextSym;
	char currentSym;

	int lineIdx;
	int columnIdx;
    token_ptr token;

	static bool isSignificant(char sym) {return sym != ' ' && sym != '\n' && sym != '\r' && sym != '\t';};
    static bool isQuote(char sym) {return sym == '\'' || sym == '"';};
    static bool isFirstIdentSym(char sym) {return isalpha(sym) || sym >= 127 || sym == '_';};
    static bool isMidIdentSym(char sym) {return isFirstIdentSym(sym) || isdigit(sym);};
    static bool isOctDigit(char sym) {return isdigit(sym) && sym < '8';};
    static bool isHexDigit(char sym) {return (isdigit(sym) || (sym >= 'a' && sym <= 'f') || (sym >= 'A' && sym <= 'F'));};
    static bool isExp(char sym) {return sym == 'e' || sym == 'E';};
    static int digitValue(char digit)
    {
        if (!isHexDigit(digit)) return 10000;
        if (digit <= '9') return digit - '0';
        if ('A' <= digit && digit <= 'F') digit += 'a' - 'A';
        return 10 + digit - 'a';
    }
	char readNextSym();
	void appendCurrentToken();
    token_ptr readString();
    token_ptr readIdent();
    std::string readNumberPart(int maxDigitAllowed);
    token_ptr readNumber();
    token_ptr singleLineComment();
    token_ptr multiLineComment();
    token_ptr readOperator();
    token_ptr readTokenAtCursor();
    void invalidToken(std::string message);

public:
    explicit Lexer(const char* sourceName)
    {
		source.open(sourceName);
		eof = !source.get(nextSym);
        lineIdx = 1;
        columnIdx = 0;
    }
    token_ptr getNext();
    token_ptr getCurrent() {return token;}
};
