#include <iostream>
#include "lexer.h"

using namespace std;

char Lexer::readNextSym()
{
    currentSym = nextSym;
    if (!source.get(nextSym)) nextSym = -1;
    if (currentSym == -1) eof = true;
    columnIdx++;
    if (currentSym == '\n')
    {
        lineIdx++;
        columnIdx = 0;
    }
}

void Lexer::appendCurrentToken()
{
    currentToken << nextSym;
    readNextSym();
}

token_ptr Lexer::readString()
{
    char quote = currentSym;
    stringstream stringSource;

    currentToken << currentSym;
    stringSource << currentSym;

    readNextSym();
    while (currentSym != quote)
    {
        if (currentSym == '\\')
        {
            char sym = -1;
            if (quote == '\'') sym = '\'';
            if (quote == '"') switch (nextSym)
            {
                case 'n' : sym = '\n'; break;
                case '"' : sym = '\"'; break;
                case '\\' : sym = '\\'; break;
                case 't' : sym = '\t'; break;
                case '0' : sym = '\0'; break;
                case 'f' : sym = '\f'; break;
                case 'v' : sym = '\v'; break;
                case 'r' : sym = '\r'; break;
            }
            if (sym != -1) currentToken << sym;
            else currentToken << currentSym << nextSym;

            stringSource << currentSym << nextSym;
            readNextSym();
        }
        else
        {
            currentToken << currentSym;
            stringSource << currentSym;
        }

        if (eof) throw InvalidToken(lineIdx, columnIdx, "Missing quote");
        readNextSym();
    }
    currentToken << currentSym;
    stringSource << currentSym;


    return token_ptr(new StringValueToken(currentToken.str(), TokenType::STRING, stringSource.str()));
}

token_ptr Lexer::readIdent()
{
    TokenType type = TokenType::IDENT;
    currentToken << currentSym;
    if (currentSym == '$')
    {
        type = TokenType::VAR;
        readNextSym();
        currentToken << currentSym;
        if (!isFirstIdentSym(currentSym))
            throw InvalidToken(lineIdx, columnIdx, "Invalid identificator begining");
    }
    while (!eof && isMidIdentSym(nextSym))
    {
        currentToken << nextSym;
        readNextSym();
    }
    auto result = keywords.find(currentToken.str());
    if (result != keywords.end()) return token_ptr(new StringValueToken(result->first, result->second, result->first));
    return token_ptr(new StringValueToken(currentToken.str(), type, currentToken.str()));
}

string Lexer::readNumberPart(int maxDigitAllowed)
{
    appendCurrentToken();
    if (digitValue(nextSym) > maxDigitAllowed) invalidToken("Invalid number");
    while (digitValue(nextSym) <= maxDigitAllowed) appendCurrentToken();
    if (isFirstIdentSym(nextSym) || isMidIdentSym(nextSym)) invalidToken("Invalid number");
    return currentToken.str();
}

token_ptr Lexer::readNumber()
{
    try {
        currentToken << currentSym;
        bool expFound = false;
        bool dotFound = false;

        if (currentSym == '0') {
            if (nextSym == 'b') {
                readNumberPart(1);
                string binStr = currentToken.str();
                return token_ptr(new IntValueToken(currentToken.str(),
                               TokenType::INTEGER, stoi(string(binStr.begin() + 2, binStr.end()), 0, 2)));
            }
            if (nextSym == 'x') return token_ptr(new IntValueToken(readNumberPart(15),
                                                                   TokenType::INTEGER, stoi(currentToken.str(), 0, 16)));
            if (isOctDigit(nextSym)) return token_ptr(new IntValueToken(readNumberPart(7),
                                                                    TokenType::INTEGER, (currentToken.str(), 0, 8)));
        }
        while (!eof && (isdigit(nextSym) || (nextSym == '.' && !dotFound && !expFound) ||
                        (isExp(nextSym) && !expFound)) || (isExp(currentSym) && (nextSym == '+' || nextSym == '-'))) {
            appendCurrentToken();
            dotFound = dotFound || currentSym == '.';
            expFound = expFound || isExp(currentSym);
            if ((nextSym == '.' && isExp(currentSym))) invalidToken("Exponent has no digits");
        }
        if (isFirstIdentSym(nextSym) || nextSym == '.') invalidToken("Invalid number ");
        if (!isdigit(currentSym)) throw InvalidToken(lineIdx, columnIdx, "Invalid number ");
        if (dotFound || expFound)
            return token_ptr(new FloatValueToken(currentToken.str(), TokenType::DECIMAL, stof(currentToken.str().c_str())));
        if (currentToken.str().size() > 19)
            throw InvalidToken(lineIdx, columnIdx, "Integer literal is too large to be represented in any integer type");
        return token_ptr(new IntValueToken(currentToken.str(), TokenType::INTEGER, stoi(currentToken.str().c_str())));
    }
    catch(std::out_of_range& e)
    {
        throw InvalidToken(lineIdx, columnIdx, "Float literal is too large");
    }
}

token_ptr Lexer::singleLineComment()
{
    string comment;
    getline(source, comment);
    currentToken << comment;
    nextSym = '\n';
    readNextSym();
    return token_ptr(new StringValueToken(currentToken.str(), TokenType::COMMENT, currentToken.str()));
}

token_ptr Lexer::multiLineComment()
{
    readNextSym();
    readNextSym();

    while (!eof)
    {
        currentToken << currentSym;
        if (currentSym == '*' && nextSym == '/')
        {
            currentToken << nextSym;
            readNextSym();
            return token_ptr(new StringValueToken(currentToken.str(), TokenType::COMMENT, currentToken.str()));
        }
        readNextSym();
    }
    throw InvalidToken(lineIdx, columnIdx, "Unterminated comment");
}

token_ptr Lexer::readOperator()
{
    currentToken << currentSym;
    if (currentSym == '#') return singleLineComment();
    if (currentSym == '/')
    {
        if (nextSym == '/') return singleLineComment();
        if (nextSym == '*') return multiLineComment();
    }

    if (currentSym == '<' && nextSym == '?')
    {
        currentToken << nextSym;
        readNextSym();
        readNextSym();
        readIdent();
        if (currentToken.str() == "<?php") return
                    (token_ptr)(new StringValueToken(currentToken.str(), TokenType::OPENING_TAG, currentToken.str()));
        else throw InvalidToken(lineIdx, columnIdx - currentToken.str().size() + 1, " opening tag expected");
    }

    auto lastValidTokenIterator = operators.find(currentToken.str());
    if (lastValidTokenIterator == operators.end())
    {
        string a = currentToken.str();
        invalidToken("Unknown symbol " + currentSym);
    }

    if (currentSym == '.' && nextSym == '.')
    {
        currentToken << nextSym;
        readNextSym();
    }
    auto lastTokenIterator = operators.find(currentToken.str() + nextSym);
    while (lastTokenIterator != operators.end())
    {
        readNextSym();
        currentToken << currentSym;
        lastValidTokenIterator = lastTokenIterator;
        lastTokenIterator = operators.find(currentToken.str() + nextSym);
    }

    return token_ptr(new StringValueToken(lastValidTokenIterator->first,
                                          lastValidTokenIterator->second, lastValidTokenIterator->first));
}

token_ptr Lexer::readTokenAtCursor()
{
    if (isdigit(currentSym)) return readNumber();
    if (isFirstIdentSym(currentSym) || (currentSym == '$')) return readIdent();
    if (isQuote(currentSym)) return readString();
    return readOperator();
}

void Lexer::invalidToken(string message)
{
    throw InvalidToken(lineIdx, columnIdx + 1, message);
}

token_ptr Lexer::getNext()
{
    currentToken.str(string());
    currentToken.clear();

    readNextSym();

    while (!eof && !isSignificant(currentSym)) readNextSym();

    int line = lineIdx;
    int column = columnIdx;

    if (eof) return (token = token_ptr(new StringValueToken("", TokenType::_EOF, "", line, column)));

    token_ptr curToken = readTokenAtCursor();
    if (curToken->type == TokenType::COMMENT) return getNext();

    curToken->line = line;
    curToken->column = column;

    token = curToken;
    return curToken;
}

