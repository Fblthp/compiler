#pragma once

#include <vector>
#include "generator.h"

class Optimizer
{
public:
    std::vector<command_ptr>& code;
    Optimizer(std::vector<command_ptr>& code);
    inline bool isNonXMMReg(operand_ptr operand);
    inline bool moveOptApplicable(operand_ptr operand1, operand_ptr operand2);
    inline bool moveAllowed(CommandType cmd);
    void movePushPop();
    void optimize();
    std::vector<std::function<bool(int)>> optimizations;
    std::stringstream res;
    const std::string nl = "//";
};