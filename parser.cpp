#include <iostream>
#include "parser.h"

using namespace std;

token_ptr Parser::getNext()
{
    try
    {
        return lexer.getNext();
    }
    catch (InvalidToken &e)
    {
        cout << e.what() << endl;
        exit(0);
    }
}
void Parser::require(TokenType type)
{
    token_ptr token = lexer.getCurrent();
    if (token->type != type) throw ParserException(token->line, token->column, tokens[(int)type] + " expected");
}

void Parser::consume(TokenType type)
{
    require(type);
    getNext();
}

void Parser::tableAppendWrapper(string value, string type)
{
    table_ptr table = currentSymbolTable;
    if (table->globals.count(value)) table = globalSymbolTable;
    table->add(value, type);
}

//tokenType is assign
void Parser::trySetType(TokenType tokenType, node_ptr left, node_ptr right)
{
    if (left->type != NodeType::VAR) return;
    string value = left->getStrValue();
    string type = determineType(right, currentSymbolTable);
    if (tokenToType.count(tokenType)) type = convertType(type, tokenToType[tokenType]);
    tableAppendWrapper(value, type);
}

node_ptr Parser::parse()
{
    getNext();
    node_ptr result(new BlockNode());
    result->table = globalSymbolTable;

    consume(TokenType::OPENING_TAG);
    token_ptr token = lexer.getCurrent();
    while(token->type != TokenType::_EOF)
    {
        node_ptr node = parseStmt();
        if (node->table == nullptr) node->table = currentSymbolTable;
        result->addChild(node);
        //TODO ?
        parseEmpty();
        token = lexer.getCurrent();
    }
    consume(TokenType::_EOF);
    return result;
}

node_ptr Parser::parseEmpty()
{
    while (lexer.getCurrent()->type == TokenType::SEMICOLON) getNext();
    return (node_ptr)(new EmptyNode());
}

node_ptr Parser::parseBlock()
{
    node_ptr block = (node_ptr)(new BlockNode());
    consume(TokenType::LEFT_BRACE);
    token_ptr token = lexer.getCurrent();
    while (token->type != TokenType::RIGHT_BRACE)
    {
        block->addChild(parseStmt());
        token = lexer.getCurrent();
    }
    consume(TokenType::RIGHT_BRACE);
    return block;
}

node_ptr Parser::parseFuncImplementation()
{
    consume(TokenType::FUNCTION);

    tableStack.push(currentSymbolTable);
    currentSymbolTable = (table_ptr)(new SymbolTable);

    require(TokenType::IDENT);

    token_ptr token = lexer.getCurrent();
    node_ptr name(new StringValueNode(token->getValueAsString()));

    getNext();
    consume(TokenType::LEFT_PARENTHESIS);
    token = lexer.getCurrent();
    vector<node_ptr> funcArgs;
    while (token->type != TokenType::RIGHT_PARENTHESIS)
    {
        // type $a1, type $a2
        if (token->type != TokenType::IDENT && !types.count(token->type))
            throw ParserException(token->line, token->column, " invalid argument type");
        string type = token->getValueAsString();
        token = getNext();
        consume(TokenType::VAR);
        currentSymbolTable->add(token->getValueAsString(), type);
        currentSymbolTable->argsOffset[token->getValueAsString()] = funcArgs.size() * 8;
        node_ptr arg(new FunctionArgumentNode(
                node_ptr(new IdentNode(type)),
                node_ptr(new VarNode(token->getValueAsString()))
        ));
        funcArgs.push_back(arg);

        if (lexer.getCurrent()->type != TokenType::RIGHT_PARENTHESIS) consume(TokenType::COMMA);
        token = lexer.getCurrent();
    }
    consume(TokenType::RIGHT_PARENTHESIS);

    //no return type is void
    node_ptr returnType;
    if (lexer.getCurrent()->type == TokenType::COLON)
    {
        token = getNext();
        if (token->type != TokenType::IDENT && !types.count(token->type))
            throw ParserException(token->line, token->column, " invalid function return type");
        returnType = node_ptr(new IdentNode(token->getValueAsString()));
        getNext();
    }
    node_ptr block = parseBlock();
    block->table = currentSymbolTable;

    node_ptr func(new FunctionNode(name, returnType, funcArgs, block));
    currentSymbolTable = tableStack.top();
    tableStack.pop();

    currentSymbolTable->addFunction(name->getStrValue(), func);
//    tableAppendWrapper(func->children[0]->token->getValueAsString(), set<string>{buildInTypes[TYPE_FUNCTION]});
    return func;

}

node_ptr Parser::parseStmt()
{
    token_ptr token = lexer.getCurrent();
    switch(token->type)
    {
        case TokenType::GLOBAL: return parseGlobal();
        case TokenType::IF:
        case TokenType::WHILE: return parseConditionable();
        case TokenType::DO: return parseDoWhile();
        case TokenType::SEMICOLON: return parseEmpty();
        case TokenType::LEFT_BRACE: return parseBlock();
        case TokenType::FOR: return parseFor();
        case TokenType::FUNCTION: return parseFuncImplementation();
        case TokenType::CLASS: return parseClass();
        case TokenType::PRINT:
        case TokenType::ECHO: return parseEcho();
    }
    node_ptr result = parseExpr(HIGHEST_PRIORITY);
    if (dynamic_pointer_cast<OperatorNode>(result) != 0 && !assigns.count((dynamic_pointer_cast<OperatorNode>(result))->operation)) result->used = false;
    consume(TokenType::SEMICOLON);
    return result;
}

node_ptr Parser::parseEcho()
{
    node_ptr node = node_ptr(new EchoNode());
    getNext();
    while (lexer.getCurrent()->type != TokenType::SEMICOLON)
    {
        node->addChild(parseExpr(HIGHEST_PRIORITY));
        if (lexer.getCurrent()->type != TokenType::SEMICOLON) consume(TokenType::COMMA);
    }
    consume(TokenType::SEMICOLON);
    return node;
}

node_ptr Parser::parseGlobal()
{
    node_ptr node = node_ptr(new BlockNode());
    consume(TokenType::GLOBAL);

    token_ptr token = lexer.getCurrent();
    while (token->type == TokenType::VAR)
    {
        node->addChild(node_ptr(new VarNode(token->getValueAsString())));
        currentSymbolTable->addGlobal(token->getValueAsString());
        if (lexer.getCurrent()->type != TokenType::SEMICOLON) consume(TokenType::COMMA);
        token = lexer.getCurrent();
    }
    consume(TokenType::SEMICOLON);
    return node;
}

node_ptr Parser::parseClass()
{
    consume(TokenType::CLASS);

    tableStack.push(currentSymbolTable);
    currentSymbolTable = table_ptr(new SymbolTable);

    require(TokenType::IDENT);
    token_ptr token = lexer.getCurrent();
    node_ptr name = node_ptr(new IdentNode(token->getValueAsString()));

    node_ptr classNode(new ClassNode(name));
    classNode->table = currentSymbolTable;
    getNext();

    consume(TokenType::LEFT_BRACE);
    while(lexer.getCurrent()->type != TokenType::RIGHT_BRACE)
    {
        token = lexer.getCurrent();
        if (lexer.getCurrent()->type == TokenType::FUNCTION)
            dynamic_pointer_cast<ClassNode>(classNode)->addMethod(parseFuncImplementation());
        else
        {
            if (lexer.getCurrent()->type == TokenType::ARRAY)
            {
                getNext();
                node_ptr arrayType = parseArray();
                string field = lexer.getCurrent()->getValueAsString();
                consume(TokenType::VAR);
                dynamic_pointer_cast<ClassNode>(classNode)->addArrayField(field, arrayType);
            }
            else
            {
                string type = tokenToType[lexer.getCurrent()->type];
                string field = getNext()->getValueAsString();
                consume(TokenType::VAR);
                dynamic_pointer_cast<ClassNode>(classNode)->addField(field, type);
            }
            consume(TokenType::SEMICOLON);
        }
    }
    consume(TokenType::RIGHT_BRACE);

    currentSymbolTable = tableStack.top();
    tableStack.pop();
    currentSymbolTable->addClass(name->getStrValue(), classNode);
    return classNode;
}

node_ptr Parser::parseCondition(bool needed)
{
    node_ptr condition = (node_ptr)(new BlockNode);
    token_ptr token = lexer.getCurrent();
    while (token->type != TokenType::SEMICOLON && token->type != TokenType::RIGHT_PARENTHESIS)
    {
        node_ptr result = parseExpr(HIGHEST_PRIORITY);
        if (dynamic_pointer_cast<OperatorNode>(result) != 0 && !assigns.count((dynamic_pointer_cast<OperatorNode>(result))->operation)) result->used = false;
        if (needed && lexer.getCurrent()->type != TokenType::COMMA) result->used = true;
        condition->addChild(result);

        token = lexer.getCurrent();
        if (token->type != TokenType::SEMICOLON && token->type != TokenType::RIGHT_PARENTHESIS) consume(TokenType::COMMA);
    }
    return condition;
}

node_ptr Parser::parseFor()
{
    getNext();
    consume(TokenType::LEFT_PARENTHESIS);

    node_ptr initStatement = parseCondition();
    consume(TokenType::SEMICOLON);
    node_ptr condition = parseCondition(true);
    consume(TokenType::SEMICOLON);
    node_ptr iterExpression = parseCondition();
    consume(TokenType::RIGHT_PARENTHESIS);

    node_ptr block = parseStmt();
    return node_ptr(new ForNode(initStatement, condition, iterExpression, block));
}

node_ptr Parser::parseDoWhile()
{
    getNext();
    require(TokenType::LEFT_BRACE);
    node_ptr body = parseBlock();
    consume(TokenType::WHILE);
    require(TokenType::LEFT_PARENTHESIS);
    node_ptr condition = parseFactor();
    return node_ptr(new DoWhileNode(condition, body));
}

node_ptr Parser::parseConditionable()
{
    token_ptr op = lexer.getCurrent();
    getNext();
    require(TokenType::LEFT_PARENTHESIS);
    node_ptr condition(parseFactor());
    node_ptr body(parseStmt());

    if (op->type == TokenType::WHILE) return node_ptr(new WhileNode(condition, body));
    token_ptr token = lexer.getCurrent();

    node_ptr elseBlock;
    if (token->type == TokenType::ELSE)
    {
        getNext();
        elseBlock = parseStmt();
    }
    return node_ptr(new IfNode(condition, body, elseBlock));
}

//TODO
node_ptr Parser::parseExpr(int priority)
{
    if (priority < LOWEST_PRIORITY) return parseFactor();
    node_ptr left = parseExpr(priority - 1);
    token_ptr token = lexer.getCurrent();


    //Ternary operator
    auto currentNodeInfo = ternaryOperators.find(token->type);
    while (currentNodeInfo != ternaryOperators.end() && currentNodeInfo->second.priority == priority)
    {
        getNext();
        node_ptr middle = parseExpr(HIGHEST_PRIORITY);
        require(TokenType::COLON);
        getNext();
        node_ptr right = parseExpr(priority - 1);
        left = node_ptr(new TerOperatorNode(token->type, left, middle, right));
        token = lexer.getCurrent();
        currentNodeInfo = ternaryOperators.find(token->type);
    }

    //binary operators
    currentNodeInfo = binaryOperators.find(token->type);
    if (currentNodeInfo != binaryOperators.end())
    {
        if (currentNodeInfo->second.associativity == Associativity::LEFT)
            while (currentNodeInfo != binaryOperators.end() && currentNodeInfo->second.priority == priority)
            {
                node_ptr right;
                getNext();
                if (token->type == TokenType::OBJECT_OPERATOR)
                {
                    right = parseFactor();
                    if (right->type != NodeType::FUNC_CALL && right->type != NodeType ::VAR)
                        throw ParserException(lexer.getCurrent()->line, lexer.getCurrent()->column, " invalid class field");
                }

                else right = token->type == TokenType::LEFT_BRACKET ?
                             (lexer.getCurrent()->type == TokenType::RIGHT_BRACKET ?
                              node_ptr(new EmptyNode()) : parseExpr(HIGHEST_PRIORITY))
                             : parseExpr(priority - 1);

                left = node_ptr(new BinOperatorNode(token->type, left, right));
                if (token->type == TokenType::LEFT_BRACKET)
                {
                    consume(TokenType::RIGHT_BRACKET);
                    //????
                    if (lexer.getCurrent()->type == TokenType::LEFT_PARENTHESIS)
                        left = node_ptr(new FuncCallNode(left, parseFuncArgs()));
                }
                token = lexer.getCurrent();
                currentNodeInfo = binaryOperators.find(token->type);
            }
        else if (currentNodeInfo->second.priority == priority)
        {
//            if (assigns.count(token->type)) tryAppendSymbolTable(left);
            getNext();
            node_ptr right = parseExpr(priority - (currentNodeInfo->second.associativity == Associativity::NO));
            if (assigns.count(token->type)) trySetType(token->type, left, right);
            return node_ptr(new BinOperatorNode(token->type, left, right));
        }
    }

    //unary postfix operators
    currentNodeInfo = postfixUnaryOperators.find(token->type);
    if (currentNodeInfo != postfixUnaryOperators.end())
        while (currentNodeInfo->second.priority == priority)
        {
            left = node_ptr(new PostfixOperatorNode(token->type, left));
            token = getNext();
            currentNodeInfo = postfixUnaryOperators.find(token->type);
        }
    return left;
}

node_ptr Parser::parseBrackets()
{
    node_ptr args = (node_ptr)(new CalledFunctionArgumentsNode());
    token_ptr token = lexer.getCurrent();
    while (token->type != TokenType::RIGHT_BRACKET)
    {
        node_ptr arg = parseExpr(HIGHEST_PRIORITY);
        token = lexer.getCurrent();
        if (token->type != TokenType::RIGHT_BRACKET) consume(TokenType::COMMA);
        token = lexer.getCurrent();
        args->addChild(arg);
    }
    consume(TokenType::RIGHT_BRACKET);
    return node_ptr(new ArrayNode(args));
}

//node_ptr Parser::parseArray()
//{
//    //instead of SplFixedArray
//    consume(TokenType::LEFT_PARENTHESIS);
//    token_ptr token = lexer.getCurrent();
//    consume(TokenType::INTEGER);
//    consume(TokenType::RIGHT_PARENTHESIS);
//    return node_ptr(new StaticArrayNode((dynamic_pointer_cast<IntValueToken>(token))->value));
//}

void Parser::parseArrayPart(vector<int>& sizes, string& type)
{
    consume(TokenType::LEFT_PARENTHESIS);

    token_ptr token = lexer.getCurrent();
    consume(TokenType::INTEGER);
    sizes.push_back((dynamic_pointer_cast<IntValueToken>(token))->value);

    if (lexer.getCurrent()->type != TokenType::RIGHT_PARENTHESIS)
    {
        consume(TokenType::COMMA);
        TokenType tokenType = lexer.getCurrent()->type;
        if (tokenType == TokenType::TYPE_FLOAT || tokenType == TokenType::TYPE_INT)
        {
            type = tokenToType[tokenType];
            getNext();
        }
        else
        {
            consume(TokenType::ARRAY);
            parseArrayPart(sizes, type);
        }
    }

    consume(TokenType::RIGHT_PARENTHESIS);
}

node_ptr Parser::parseArray()
{
    //instead of SplFixedArray
    vector<int> sizes;
    string type = buildInTypes[TYPE_INT];

    parseArrayPart(sizes, type);

    return node_ptr(new StaticArrayNode(sizes, type));
}

node_ptr Parser::parseFactor()
{
    token_ptr token = lexer.getCurrent();
    token_ptr nextToken = getNext();

    if (token->type == TokenType::RETURN && nextToken->type == TokenType::SEMICOLON)
        return node_ptr(new PrefixOperatorNode(token->type, node_ptr(new EmptyNode())));

    auto currentNodeInfo = prefixUnaryOperators.find(token->type);
    if (currentNodeInfo != prefixUnaryOperators.end())
        return node_ptr(new PrefixOperatorNode(token->type, parseExpr(currentNodeInfo->second.priority)));

    if (token->type == TokenType::LEFT_BRACKET) return parseBrackets();
    if (token->type == TokenType::LEFT_PARENTHESIS)
    {
// TODO type cast
//        if (types.count(lexer.getCurrent()->type))
//        {
//            token_ptr type = lexer.getCurrent();
//            lexer.getNext();
//            consume(TokenType::RIGHT_PARENTHESIS);
//            node_ptr value = parseExpr(TYPE_PRIORITY);
//            return (node_ptr)(new UnOpNode(type, value));
//        }
        node_ptr node = parseExpr(HIGHEST_PRIORITY);
        consume(TokenType::RIGHT_PARENTHESIS);
        return node;
    }

    if (token->type == TokenType::ARRAY) return parseArray();

    if (token->type == TokenType::IDENT && nextToken->type == TokenType::LEFT_PARENTHESIS)
        return node_ptr(new FuncCallNode(node_ptr(new IdentNode(token->getValueAsString())), parseFuncArgs()));

    switch(token->type)
    {
        case TokenType::IDENT:
            return node_ptr(new IdentNode(token->getValueAsString()));
        case TokenType::VAR:
            return node_ptr(new VarNode(token->getValueAsString()));
        case TokenType::STRING:
            return node_ptr(new StringValueNode(token->getValueAsString()));
        case TokenType::INTEGER:
            return node_ptr(new IntValueNode((dynamic_pointer_cast<IntValueToken>(token))->value));
        case TokenType::DECIMAL:
            return node_ptr(new FloatValueNode((dynamic_pointer_cast<FloatValueToken>(token))->value));
        case TokenType::LEFT_BRACKET: return parseBrackets();
    }

    throw ParserException(token->line, token->column, "Unexpected token");
}

node_ptr Parser::parseFuncArgs()
{
    consume(TokenType::LEFT_PARENTHESIS);
    node_ptr args = (node_ptr)(new CalledFunctionArgumentsNode());
    token_ptr token = lexer.getCurrent();
    while (token->type != TokenType::RIGHT_PARENTHESIS)
    {
        node_ptr arg = parseExpr(HIGHEST_PRIORITY);
        token = lexer.getCurrent();
        if (token->type != TokenType::RIGHT_PARENTHESIS) consume(TokenType::COMMA);
        token = lexer.getCurrent();
        args->addChild(arg);
    }
    consume(TokenType::RIGHT_PARENTHESIS);
    return args;
}