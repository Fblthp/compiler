#include "generator.h"
#include <iostream>

using namespace std;

void Generator::init()
{
    code << ".cstring" << "\n";
    code << ".text" << "\n";
    code << ".globl _main" << "\n";
    
    code << INT_FMT_LBL << ":" << "\n";
    code << ".ascii \"%d" << "\\" << "0" << "\"" << "\n";

    code << FLOAT_FMT_LBL << ":" << "\n";
    code << ".ascii \"%.5f" << "\\" << "0" << "\"" << "\n";

    code << "_main:" << "\n";
    addLine(CommandType::PUSH, RBP_REG);
}

void Generator::finish()
{
    addLine(CommandType::POP, RBP_REG);
    addLine(CommandType::RET);

    for (int i = 0; i < asmCode.size(); ++i) code << asmCode[i]->repr;

    for (auto it = stringsToLabels.begin(); it != stringsToLabels.end(); ++it)
    {
        code << it->second << ":" << "\n";
        code << ".string " << "\"" << it->first.substr(1, it->first.size() - 2) << "\\" << "0" << "\"" << "\n";
    }

    code << ".data" << "\n";
    for (auto it = glTable->table.begin(); it != glTable->table.end(); ++it)
    {
        if (typeToAsm.count(it->second))
            code << GL_VAR_PREF + it->first << ": " << typeToAsm[it->second] << " 0 " << "\n";
    }

    for (auto it = arraySizes.begin(); it != arraySizes.end(); ++it)
        code << GL_VAR_PREF + it->first << ": .zero " << it->second << "\n";
}

void Generator::addLine(CommandType type)
{
//    code << singleCommands[type].repr << "\n";
    asmCode.push_back(command_ptr(new Command(type)));
}

void Generator::addLine(CommandType type, operand_ptr arg)
{
    auto commandInfo = unCommands[type];
//    code << commandInfo.repr << " " << arg << "\n";
    asmCode.push_back(command_ptr(new Command(type, arg)));
}

void Generator::addLine(CommandType type, Reg reg)
{
    asmCode.push_back(command_ptr(new Command(type, operand_ptr(new Register(registers.at(reg))))));
}

void Generator::addLine(CommandType type, string arg)
{
    asmCode.push_back(command_ptr(new Command(type, operand_ptr(new ConstantOperand(arg)))));
}

void Generator::addLine(CommandType type, Reg reg1, Reg reg2)
{
    auto commandInfo = binCommands[type];
//    code << commandInfo.repr << " " << arg1 << ", " << arg2 << "\n";
    asmCode.push_back(command_ptr(new Command(type, operand_ptr(new Register(registers.at(reg1))), operand_ptr(new Register(registers.at(reg2))))));
}

void Generator::addLine(CommandType type, Reg reg, operand_ptr arg)
{
    asmCode.push_back(command_ptr(new Command(type, operand_ptr(new Register(registers.at(reg))), arg)));
}

void Generator::addLine(CommandType type, operand_ptr arg, Reg reg)
{
    asmCode.push_back(command_ptr(new Command(type, arg, operand_ptr(new Register(registers.at(reg))))));
}

void Generator::addLine(CommandType type, operand_ptr arg1, operand_ptr arg2)
{
    asmCode.push_back(command_ptr(new Command(type, arg1, arg2)));
}

void Generator::addLabel(std::string lbl)
{
    addLine(CommandType::LBL, lbl + ":");
}

operand_ptr Generator::getAdr(string lbl)
{
    return operand_ptr(new VarAdress(lbl));
}

operand_ptr Generator::getArrayAdr(Reg reg1, Reg reg2)
{
    return operand_ptr(new OffsetOperand(registers.at(reg1), registers.at(reg2)));
}

void Generator::callPrintf()
{
    addLine(CommandType::CALL, "_printf");
}

void Generator::popReq(Reg reg, std::string type)
{
    //TODO
    if (reg == XMM0_REG || reg == XMM1_REG)
    {
        addLine(CommandType::POP, RAX_REG);
        if (type == buildInTypes[TYPE_INT])
        {
            operand_ptr addr = operand_ptr(new RBPAddr());
            addLine(CommandType::MOVQ, RAX_REG, addr);
            addLine(CommandType::CVTSI2SSL, addr, reg);
            addLine(CommandType::CVTSS2SD, reg, reg);
            return;
        }
        addLine(CommandType::MOVQ, RAX_REG, reg);
        addLine(CommandType::MOVB, intToOperand(1), AL_REG);
    }
    else addLine(CommandType::POP, reg);
}

void Generator::pushReq(Reg reg)
{
    //TODO
    if (reg == XMM0_REG || reg == XMM1_REG)
    {
        addLine(CommandType::MOVQ, reg, RAX_REG);
        addLine(CommandType::PUSH, RAX_REG);
    }
    else addLine(CommandType::PUSH, reg);
}

string Generator::generateNextLbl()
{
    for (int i = lastLbl.size() - 1; i >= 0; --i)
    {
        if (lastLbl[i] != 'z')
        {
            lastLbl[i]++;
            return lastLbl;
        }
    }
    lastLbl = string(lastLbl.size() + 1, 'a');
    return LBL_PREF + lastLbl;
}

Generator generator;

