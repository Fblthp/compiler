#pragma once

#include <string>
#include <vector>
#include <map>
#include <set>

const int LOWEST_PRIORITY = 8;
const int HIGHEST_PRIORITY = 32;
const int TYPE_PRIORITY = 12;
const int ASSIGN_PRIORITY = 30;
const int OBJECT_OPERATOR_PRIORITY = 10;

enum class Associativity : int
{
    LEFT,
    RIGHT,
    NO
};

struct NodeInfo
{
    std::string name;
    int priority;
    Associativity associativity;
    NodeInfo(){};
    NodeInfo(std::string name) : name(name){};
    NodeInfo(std::string name, int priority, Associativity associativity) : name(name),
                                                priority(priority), associativity(associativity){};
};

enum class TokenType : int
{
    UNDEFINED,
    STRING,
    INTEGER,
    HEX,
    BIN,
    OCT,
    DECIMAL,
    VAR,
    IDENT,
    COMMENT,

    LEFT_PARENTHESIS,
    RIGHT_PARENTHESIS,
    LEFT_BRACKET,
    RIGHT_BRACKET,
    LEFT_BRACE,
    RIGHT_BRACE,

    MINUS,
    ASSIGN_MINUS,
    PLUS,
    ASSIGN_PLUS,
    DIV,
    ASSIGN_DIV,
    MULT,
    ASSIGN_MULT,

    INC,
    DEC,

    MOD,
    ASSIGN_MOD,
    POWER,
    ASSIGN_POWER,

    CONCAT,
    ASSIGN_CONCAT,

    ASSIGN,
    NOT,
    NOT_EQUAL,
    NOT_IEQUAL,
    EQUAL,
    IEQUAL,
    GREATER,
    GREATER_EQUAL,
    LESS,
    LESS_EQUAL,
    SPACESHIP,

    OR,
    ASSIGN_OR,
    AND,
    ASSIGN_AND,
    BIT_OR,
    ASSIGN_BIT_OR,
    BIT_AND,
    ASSIGN_BIT_AND,
    XOR,
    BIT_XOR,
    ASSIGN_BIT_XOR,

    SHR,
    ASSIGN_SHR,
    SHL,
    ASSIGN_SHL,

    COMMA,
    SEMICOLON,
    COLONS,

    TERNARY,
    COLON,
    BIT_NOT,
    ERROR, // @
    OBJECT_OPERATOR, // ->
    ARROW, // =>
    SLASH,
    ASSIGN_REFERENCE,
    NULL_COALESCE, // ??
    _EOF,

    IF,
    ELSE,
    ELSEIF,
    WHILE,
    FOR,
    ENDFOR,
    FOREACH,
    ENDFOREACH,
    ENDIF,
    ENDSWITCH,
    ENDWHILE,

    CONTINUE,
    BREAK,
    EXIT,

    SWITCH,
    CASE,

    RETURN,
    CONST,
    DO,

    PRIVATE,
    PROTECTED,
    PUBLIC,
    STATIC,
    GLOBAL,
    ABSTRACT,
    CALLABLE,

    TRY,
    CATCH,
    THROW,

    USE,
    _VAR,

    INSTANCEOF,
    AS,

    CLONE,
    NEW,

    ECHO,
    PRINT,

    ARRAY,
    LIST,
    FUNCTION,
    CLASS,

    EXTENDS,
    IMPLEMENTS,

    NAMESPACE,
    INCLUDE,
    INCLUDE_ONCE,
    DECLARE,
    ENDDECLARE,
    REQUIRE,

    DEFAULT,
    DIE,
    EMPTY,
    OPENING_TAG,

    TYPE_INT,
    TYPE_BOOL,
    TYPE_FLOAT,
    TYPE_STRING,
    OBJECT,
    UNSET,

    ELLIPSIS,
    FINALLY,
    REQUIRE_ONCE,
    YIELD,
    INTERFACE,
    FINAL,
    TRAIT
};

static std::map<std::string, TokenType> keywords =
{
    {"and", TokenType::AND},
    {"or", TokenType::OR},
    {"xor", TokenType::XOR},

    {"if", TokenType::IF},
    {"else", TokenType::ELSE},
    {"elseif", TokenType::ELSEIF},

    {"while", TokenType::WHILE},
    {"for", TokenType::FOR},
    {"endfor", TokenType::ENDFOR},
    {"foreach", TokenType::FOREACH},
    {"endforeach", TokenType::ENDFOREACH},
    {"endif", TokenType::ENDIF},
    {"endswitch", TokenType::ENDSWITCH},
    {"endwhile", TokenType::ENDWHILE},

    {"continue", TokenType::CONTINUE},
    {"break", TokenType::BREAK},
//    {"exit", TokenType::EXIT},

    {"switch", TokenType::SWITCH},
    {"case", TokenType::CASE},

    {"return", TokenType::RETURN},
    {"const", TokenType::CONST},
    {"do", TokenType::DO},

    {"private", TokenType::PRIVATE},
    {"protected", TokenType::PROTECTED},
    {"public", TokenType::PUBLIC},
    {"static", TokenType::STATIC},
    {"global", TokenType::GLOBAL},
    {"abstract", TokenType::ABSTRACT},
    {"final", TokenType::FINAL},
//    {"callable", TokenType::CALLABLE},

    {"try", TokenType::TRY},
    {"catch", TokenType::CATCH},
    {"finally", TokenType::FINALLY},
    {"throw", TokenType::THROW},

    {"use", TokenType::USE},
    {"var", TokenType::_VAR},

    {"instanceof", TokenType::INSTANCEOF},
    {"as", TokenType::AS},

    {"clone", TokenType::CLONE},
    {"new", TokenType::NEW},

    {"echo", TokenType::ECHO},
    {"print", TokenType::PRINT},

    {"array", TokenType::ARRAY},
//    {"list", TokenType::LIST},
    {"function", TokenType::FUNCTION},
    {"class", TokenType::CLASS},

    {"extends", TokenType::EXTENDS},
    {"implements", TokenType::IMPLEMENTS},

    {"namespace", TokenType::NAMESPACE},
    {"include", TokenType::INCLUDE},
    {"include_once", TokenType::INCLUDE_ONCE},
    {"declare", TokenType::DECLARE},
    {"enddeclare", TokenType::ENDDECLARE},
    {"require", TokenType::REQUIRE},
    {"require_once", TokenType::REQUIRE_ONCE},

    {"default", TokenType::DEFAULT},
//    {"die", TokenType::DIE},
//    {"empty", TokenType::EMPTY},

    {"int", TokenType::TYPE_INT},
    {"integer", TokenType::TYPE_INT},
    {"bool", TokenType::TYPE_BOOL},
    {"boolean", TokenType::TYPE_BOOL},
    {"float", TokenType::TYPE_FLOAT},
    {"double", TokenType::TYPE_FLOAT},
    {"real", TokenType::TYPE_FLOAT},
    {"string", TokenType::TYPE_STRING},
    {"object", TokenType::OBJECT},
    {"yield", TokenType::YIELD},
    {"interface", TokenType::INTERFACE},
    {"trait", TokenType::TRAIT},
//    {"unset", TokenType::UNSET}

};

static std::map<std::string, TokenType> operators =
{
    {"(", TokenType::LEFT_PARENTHESIS},
    {")", TokenType::RIGHT_PARENTHESIS},
    {"[", TokenType::LEFT_BRACKET},
    {"]", TokenType::RIGHT_BRACKET},
    {"{", TokenType::LEFT_BRACE},
    {"}", TokenType::RIGHT_BRACE},

    {"-", TokenType::MINUS},
    {"-=", TokenType::ASSIGN_MINUS},
    {"+", TokenType::PLUS},
    {"+=", TokenType::ASSIGN_PLUS},
    {"/", TokenType::DIV},
    {"/=", TokenType::ASSIGN_DIV},
    {"*", TokenType::MULT},
    {"*=", TokenType::ASSIGN_MULT},

    {"++", TokenType::INC},
    {"--", TokenType::DEC},

    {"%", TokenType::MOD},
    {"%=", TokenType::ASSIGN_MOD},
    {"**", TokenType::POWER},
    {"**=", TokenType::ASSIGN_POWER},

    {".", TokenType::CONCAT},
    {".=", TokenType::ASSIGN_CONCAT},

    {"=", TokenType::ASSIGN},
    {"!", TokenType::NOT},
    {"!=", TokenType::NOT_EQUAL},
    {"<>", TokenType::NOT_EQUAL},
    {"!==", TokenType::NOT_IEQUAL},
    {"==", TokenType::EQUAL},
    {"===", TokenType::IEQUAL},
    {">", TokenType::GREATER},
    {">=", TokenType::GREATER_EQUAL},
    {"<", TokenType::LESS},
    {"<=", TokenType::LESS_EQUAL},
    {"<=>", TokenType::SPACESHIP},

    {"||", TokenType::OR},
//  {"||=", TokenType::ASSIGN_OR},
    {"&&", TokenType::AND},
//  {"&&=", TokenType::ASSIGN_AND},
    {"|", TokenType::BIT_OR},
    {"|=", TokenType::ASSIGN_BIT_OR},
    {"&", TokenType::BIT_AND},
    {"&=", TokenType::ASSIGN_BIT_AND},
    {"^", TokenType::BIT_XOR},
    {"^=", TokenType::ASSIGN_BIT_XOR},

    {">>", TokenType::SHR},
    {">>=", TokenType::ASSIGN_SHR},
    {"<<", TokenType::SHL},
    {"<<=", TokenType::ASSIGN_SHL},

    {",", TokenType::COMMA},
    {";", TokenType::SEMICOLON},
    {"::", TokenType::COLONS},

    {"?", TokenType::TERNARY},
    {":", TokenType::COLON},
    {"~", TokenType::BIT_NOT},
    {"@", TokenType::ERROR},

    {"->", TokenType::OBJECT_OPERATOR},
    {"=>", TokenType::ARROW},
    {"\\", TokenType::SLASH},
    {"=&", TokenType::ASSIGN_REFERENCE},
    {"??", TokenType::NULL_COALESCE},
    {"...", TokenType::ELLIPSIS}
};

static std::vector<std::string> tokens =
{
    "UNDEFINED",

    "STRING",
    "INTEGER",
    "HEX",
    "BIN",
    "OCT",
    "DECIMAL",
    "VAR",
    "IDENT",
    "COMMENT",

    "LEFT_PARENTHESIS",
    "RIGHT_PARENTHESIS",
    "LEFT_BRACKET",
    "RIGHT_BRACKET",
    "LEFT_BRACE",
    "RIGHT_BRACE",

    "MINUS",
    "ASSIGN_MINUS",
    "PLUS",
    "ASSIGN_PLUS",
    "DIV",
    "ASSIGN_DIV",
    "MULT",
    "ASSIGN_MULT",

    "INC",
    "DEC",

    "MOD",
    "ASSIGN_MOD",
    "POWER",
    "ASSIGN_POWER",

    "CONCAT",
    "ASSIGN_CONCAT",

    "ASSIGN",
    "NOT",
    "NOT_EQUAL",
    "NOT_IEQUAL",
    "EQUAL",
    "IEQUAL",
    "GREATER",
    "GREATER_EQUAL",
    "LESS",
    "LESS_EQUAL",
    "SPACESHIP",

    // TODO operators 'and' and 'or' have different associativity from && and ||
    "OR",
    "ASSIGN_OR",
    "AND",
    "ASSIGN_AND",
    "BIT_OR",
    "ASSIGN_BIT_OR",
    // TODO REFERENCE
    "BIT_AND",
    "ASSIGN_BIT_AND",
    "XOR",
    "BIT_XOR",
    "ASSIGN_BIT_XOR",

    "SHR",
    "ASSIGN_SHR",
    "SHL",
    "ASSIGN_SHL",

    "COMMA",
    "SEMICOLON",
    "COLONS",

    "TERNARY",
    "COLON",
    "BIT_NOT",
    "ERROR",
    "OBJECT_OPERATOR",
    "ARROW",
    "SLASH",
    "ASSIGN_REFERENCE",
    "NULL_COALESCE",
    "EOF",

    "IF",
    "ELSE",
    "ELSEIF",
    "WHILE",
    "FOR",
    "ENDFOR",
    "FOREACH",
    "ENDFOREACH",
    "ENDIF",
    "ENDSWITCH",
    "ENDWHILE",

    "CONTINUE",
    "BREAK",
    "EXIT",

    "SWITCH",
    "CASE",

    "RETURN",
    "CONST",
    "DO",

    "PRIVATE",
    "PROTECTED",
    "PUBLIC",
    "STATIC",
    "GLOBAL",
    "ABSTRACT",
    "CALLABLE",

    "TRY",
    "CATCH",
    "THROW",

    "USE",
    "_VAR",

    "INSTANCEOF",
    "AS",

    "CLONE",
    "NEW",

    "ECHO",
    "PRINT",

    "ARRAY",
    "LIST",
    "FUNCTION",
    "CLASS",

    "EXTENDS",
    "IMPLEMENTS",

    "NAMESPACE",
    "INCLUDE",
    "INCLUDE_ONCE",
    "DECLARE",
    "ENDDECLARE",
    "REQUIRE",

    "DEFAULT",
    "DIE",
    "EMPTY",
    "OPENING_TAG",

    "TYPE_INT",
    "TYPE_BOOL",
    "TYPE_FLOAT",
    "TYPE_STRING",
    "OBJECT",
    "UNSET",

    "ELLIPSIS",
    "FINALLY",
    "REQUIRE_ONCE",
    "YIELD",
    "INTERFACE",
    "FINAL",
    "TRAIT"
};

static std::map<TokenType, NodeInfo> binaryOperators =
{
    {TokenType::LEFT_BRACKET, NodeInfo("LEFT_BRACKET", 10, Associativity::LEFT)},

    {TokenType::MINUS, NodeInfo("MINUS", 16, Associativity::LEFT)},
    {TokenType::ASSIGN_MINUS, NodeInfo("ASSIGN_MINUS", 30, Associativity::RIGHT)},
    {TokenType::PLUS, NodeInfo("PLUS", 16, Associativity::LEFT)},
    {TokenType::ASSIGN_PLUS, NodeInfo("ASSIGN_PLUS", 30, Associativity::RIGHT)},
    {TokenType::DIV, NodeInfo("DIV", 15, Associativity::LEFT)},
    {TokenType::ASSIGN_DIV, NodeInfo("ASSIGN_DIV", 30, Associativity::RIGHT)},
    {TokenType::MULT, NodeInfo("MULT", 15, Associativity::LEFT)},
    {TokenType::ASSIGN_MULT, NodeInfo("ASSIGN_MULT", 30, Associativity::RIGHT)},

    {TokenType::MOD, NodeInfo("MOD", 15, Associativity::LEFT)},
    {TokenType::ASSIGN_MOD, NodeInfo("ASSIGN_MOD", 30, Associativity::RIGHT)},
    {TokenType::POWER, NodeInfo("POWER", 11, Associativity::RIGHT)},
    {TokenType::ASSIGN_POWER, NodeInfo("ASSIGN_POWER", 30, Associativity::RIGHT)},

    {TokenType::CONCAT, NodeInfo("CONCAT", 16, Associativity::LEFT)},
    {TokenType::ASSIGN_CONCAT, NodeInfo("ASSIGN_CONCAT", 30, Associativity::RIGHT)},

    {TokenType::ASSIGN, NodeInfo("ASSIGN", 30, Associativity::RIGHT)},
    {TokenType::NOT_EQUAL, NodeInfo("NOT_EQUAL", 19, Associativity::NO)},
    {TokenType::NOT_IEQUAL, NodeInfo("NOT_IEQUAL", 19, Associativity::NO)},
    {TokenType::EQUAL, NodeInfo("EQUAL", 19, Associativity::NO)},
    {TokenType::IEQUAL, NodeInfo("IEQUAL", 19, Associativity::NO)},
    {TokenType::GREATER, NodeInfo("GREATER", 18, Associativity::NO)},
    {TokenType::GREATER_EQUAL, NodeInfo("GREATER_EQUAL", 18, Associativity::NO)},
    {TokenType::LESS, NodeInfo("LESS", 18, Associativity::NO)},
    {TokenType::LESS_EQUAL, NodeInfo("LESS_EQUAL", 18, Associativity::NO)},
    {TokenType::SPACESHIP, NodeInfo("SPACESHIP", 19, Associativity::NO)},

    {TokenType::OR, NodeInfo("OR", 24, Associativity::LEFT)},
    {TokenType::ASSIGN_OR, NodeInfo("ASSIGN_OR", 30, Associativity::RIGHT)},
    {TokenType::AND, NodeInfo("AND", 23, Associativity::LEFT)},
    {TokenType::ASSIGN_AND, NodeInfo("ASSIGN_AND", 30, Associativity::RIGHT)},
    {TokenType::BIT_OR, NodeInfo("BIT_OR", 22, Associativity::LEFT)},
    {TokenType::ASSIGN_BIT_OR, NodeInfo("ASSIGN_BIT_OR", 30, Associativity::RIGHT)},
    {TokenType::BIT_AND, NodeInfo("BIT_AND", 20, Associativity::LEFT)},
    {TokenType::ASSIGN_BIT_AND, NodeInfo("ASSIGN_BIT_AND", 30, Associativity::RIGHT)},
    {TokenType::XOR, NodeInfo("XOR", 31, Associativity::LEFT)},
    {TokenType::BIT_XOR, NodeInfo("BIT_XOR", 21, Associativity::LEFT)},
    {TokenType::ASSIGN_BIT_XOR, NodeInfo("ASSIGN_BIT_XOR", 30, Associativity::RIGHT)},

    {TokenType::SHR, NodeInfo("SHR", 17, Associativity::LEFT)},
    {TokenType::ASSIGN_SHR, NodeInfo("ASSIGN_SHR", 30, Associativity::RIGHT)},
    {TokenType::SHL, NodeInfo("SHL", 17, Associativity::LEFT)},
    {TokenType::ASSIGN_SHL, NodeInfo("ASSIGN_SHL", 30, Associativity::RIGHT)},

    {TokenType::OBJECT_OPERATOR, NodeInfo("OBJECT_OPERATOR", 10, Associativity::LEFT)},
    {TokenType::NULL_COALESCE, NodeInfo("NULL_COALESCE", 25, Associativity::RIGHT)},
    {TokenType::INSTANCEOF, NodeInfo("INSTANCE_OF", 13, Associativity::NO)},
    {TokenType::COLONS, NodeInfo("NAMESPACE", 8, Associativity::NO)}
};

static std::map<TokenType, NodeInfo> prefixUnaryOperators =
{
    {TokenType::MINUS, NodeInfo("UNARY_MINUS", 12, Associativity::RIGHT)},
    {TokenType::PLUS, NodeInfo("UNARY_PLUS", 12, Associativity::RIGHT)},
    {TokenType::INC, NodeInfo("PREFIX_INC", 12, Associativity::RIGHT)},
    {TokenType::DEC, NodeInfo("PREFIX_DEC", 12, Associativity::RIGHT)},

    {TokenType::NOT, NodeInfo("NOT", 14, Associativity::RIGHT)},
    {TokenType::BIT_AND, NodeInfo("REFERENCE", 20, Associativity::RIGHT)},
    {TokenType::BIT_NOT, NodeInfo("BIT_NOT", 12, Associativity::RIGHT)},
    {TokenType::ERROR, NodeInfo("ERROR", 12, Associativity::RIGHT)},
    {TokenType::THROW, NodeInfo("THROW", 32, Associativity::NO)},
    {TokenType::RETURN, NodeInfo("RETURN", 32, Associativity::NO)},
    {TokenType::YIELD, NodeInfo("YIELD", 32, Associativity::NO)},
    {TokenType::REQUIRE, NodeInfo("REQUIRE", 32, Associativity::NO)},
    {TokenType::INCLUDE, NodeInfo("INCLUDE", 32, Associativity::NO)},
    {TokenType::REQUIRE_ONCE, NodeInfo("REQUIRE_ONCE", 32, Associativity::NO)},
    {TokenType::NEW, NodeInfo("NEW", 9, Associativity::NO)},
    {TokenType::CLONE, NodeInfo("CLONE", 9, Associativity::NO)}

};

static std::map<TokenType, NodeInfo> postfixUnaryOperators =
{
    {TokenType::INC, NodeInfo("POSTFIX_INC", 12, Associativity::RIGHT)},
    {TokenType::DEC, NodeInfo("POSTFIX_DEC", 12, Associativity::RIGHT)},
};

static std::map<TokenType, NodeInfo> ternaryOperators =
{
    {TokenType::TERNARY, NodeInfo("TERNARY", 26, Associativity::LEFT)},
};

static std::map<TokenType, NodeInfo> factors =
{
    {TokenType::IDENT, NodeInfo("IDENT", -1, Associativity::NO)},
    {TokenType::VAR, NodeInfo("VAR", -1, Associativity::NO)},
    {TokenType::STRING, NodeInfo("STRING", -1, Associativity::NO)},
    {TokenType::INTEGER, NodeInfo("INTEGER", -1, Associativity::NO)},
    {TokenType::DECIMAL, NodeInfo("DECIMAL", -1, Associativity::NO)},
};

static std::set<TokenType> types =
{
    TokenType::TYPE_INT,
    TokenType::TYPE_BOOL,
    TokenType::TYPE_FLOAT,
    TokenType::TYPE_STRING,
    TokenType::ARRAY,
    TokenType::OBJECT,
    TokenType::FUNCTION,
    TokenType::UNSET
};

static std::set<TokenType> assigns =
{
    TokenType::ASSIGN,
    TokenType::ASSIGN_BIT_AND,
    TokenType::ASSIGN_BIT_OR,
    TokenType::ASSIGN_BIT_XOR,
    TokenType::ASSIGN_CONCAT,
    TokenType::ASSIGN_DIV,
    TokenType::ASSIGN_MINUS,
    TokenType::ASSIGN_MOD,
    TokenType::ASSIGN_MULT,
    TokenType::ASSIGN_PLUS,
    TokenType::ASSIGN_POWER,
    TokenType::ASSIGN_REFERENCE,
    TokenType::ASSIGN_SHL,
    TokenType::ASSIGN_SHR
};

static std::map<TokenType, TokenType> assignOperations
{
    {TokenType::ASSIGN_BIT_AND, TokenType::BIT_AND},
    {TokenType::ASSIGN_BIT_OR, TokenType::BIT_OR},
    {TokenType::ASSIGN_BIT_XOR, TokenType::BIT_XOR},
    {TokenType::ASSIGN_CONCAT, TokenType::CONCAT},
    {TokenType::ASSIGN_DIV, TokenType::DIV},
    {TokenType::ASSIGN_MINUS, TokenType::MINUS},
    {TokenType::ASSIGN_MOD, TokenType::MOD},
    {TokenType::ASSIGN_MULT, TokenType::MULT},
    {TokenType::ASSIGN_PLUS, TokenType::PLUS},
    {TokenType::ASSIGN_POWER, TokenType::POWER},
    {TokenType::ASSIGN_SHL, TokenType::SHL},
    {TokenType::ASSIGN_SHR, TokenType::SHR}
};

static const int ACCESS_MOD_IDX = 0;
static const int ABSTRACT_MOD_IDX = 1;
static const int STATIC_MOD_IDX = 2;

enum class StaticModifier : int
{
    NONE,
    STATIC
};

enum class AbstractModifier : int
{
    NONE,
    ABSTRACT
};

enum class AccessTypeModifier : int
{
    NONE,
    PRIVATE,
    PROTECTED,
    PUBLIC
};

struct FuncModifiers
{
    std::vector<int> funcModifiers = {(int)AccessTypeModifier::NONE, (int)AbstractModifier::NONE, (int)StaticModifier::NONE};
    FuncModifiers(){};
    bool isNone(int idx)
    {
        return funcModifiers[idx] == 0;
    }
};

static std::map<TokenType, int> modifiers =
{
    {TokenType::ABSTRACT, ABSTRACT_MOD_IDX},
    {TokenType::STATIC, STATIC_MOD_IDX},
    {TokenType::PRIVATE, ACCESS_MOD_IDX},
    {TokenType::PUBLIC, ACCESS_MOD_IDX},
    {TokenType::PROTECTED, ACCESS_MOD_IDX}
};

static std::map<TokenType, int> modifierValues =
{
    {TokenType::ABSTRACT, (int)AbstractModifier::ABSTRACT},
    {TokenType::STATIC, (int)StaticModifier::STATIC},
    {TokenType::PRIVATE, (int)AccessTypeModifier::PRIVATE},
    {TokenType::PUBLIC, (int)AccessTypeModifier::PUBLIC},
    {TokenType::PROTECTED, (int)AccessTypeModifier::PROTECTED}
};