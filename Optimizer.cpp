#include <iostream>
#include "optimizer.h"

using namespace std;

inline bool Optimizer::isNonXMMReg(operand_ptr operand)
{
    return dynamic_pointer_cast<Register>(operand) != 0 && operand->getValue() != registers.at(XMM0_REG) && operand->getValue() != registers.at(XMM1_REG);
}

inline bool Optimizer::moveOptApplicable(operand_ptr operand1, operand_ptr operand2)
{
    if (dynamic_pointer_cast<FloatOperand>(operand1) != 0) return isNonXMMReg(operand2);
    if (dynamic_pointer_cast<VarAdress>(operand1) != 0) return dynamic_pointer_cast<VarAdress>(operand2) == 0;
//    if (dynamic_pointer_cast<ConstantOperand>(operand1) != 0) return dynamic_pointer_cast<RBPAddr>(operand2) == 0;
    return true;
}

Optimizer::Optimizer(std::vector<command_ptr>& code) : code(code)
{
    optimizations.push_back([&](int line) -> bool
        {
            if (code[line]->cmd == CommandType::PUSH && code[line + 1]->cmd == CommandType::POP &&
                    code[line]->operands[0]->getValue() == code[line + 1]->operands[0]->getValue())
                {
                    res << nl << code[line]->repr << nl << code[line + 1]->repr << nl << "-->" << endl;
                    code.erase(code.begin() + line, code.begin() + line + 2);
                    return true;
                }
            return false;
        }
    );

    optimizations.push_back([&](int line) -> bool
        {
            if (code[line]->cmd == CommandType::PUSH && code[line + 1]->cmd == CommandType::POP &&
                code[line]->operands[0]->getValue() != code[line + 1]->operands[0]->getValue())
            {
                res << nl << code[line]->repr << nl << code[line + 1]->repr << nl << "-->" << endl;
                code[line + 1] = command_ptr(new Command(CommandType::MOVQ, code[line]->operands[0], code[line + 1]->operands[0]));
                res << nl << code[line + 1]->repr << endl;
                code.erase(code.begin() + line);
                return true;
            }
            return false;
        }
    );

    optimizations.push_back([&](int line) -> bool
        {
            if (code[line]->cmd == CommandType::MOVQ && code[line + 1]->cmd == CommandType::MOVQ &&
                code[line]->operands[1]->getValue() == code[line + 1]->operands[0]->getValue() &&
                    moveOptApplicable(code[line]->operands[0], code[line + 1]->operands[1]))
            {
                res << nl << code[line]->repr << nl << code[line + 1]->repr << nl << "-->" << endl;
                code[line + 1] = command_ptr(new Command(CommandType::MOVQ, code[line]->operands[0], code[line + 1]->operands[1]));
                res << nl << code[line + 1]->repr << endl;
                code.erase(code.begin() + line);
                return true;
            }
            return false;
        }
    );

    optimizations.push_back([&](int line) -> bool
        {
            if (code[line]->cmd == CommandType::LEAQ && code[line + 1]->cmd == CommandType::MOVQ &&
                code[line]->operands[1]->getValue() == code[line + 1]->operands[0]->getValue())
            {
                res << nl << code[line]->repr << nl << code[line + 1]->repr << nl << "-->" << endl;
                code[line + 1] = command_ptr(new Command(CommandType::LEAQ, code[line]->operands[0], code[line + 1]->operands[1]));
                res << nl << code[line + 1]->repr << endl;
                code.erase(code.begin() + line);
                return true;
            }
            return false;
        }
    );

    optimizations.push_back([&](int line) -> bool
        {
            if ((code[line]->cmd == CommandType::ADD || code[line]->cmd == CommandType::SUB) &&
                code[line]->operands[0]->getValue() == "$0")
            {
                res << nl << code[line]->repr << nl << "-->" << endl;
                code.erase(code.begin() + line);
                return true;
            }
            return false;
        }
    );
};

inline bool Optimizer::moveAllowed(CommandType cmd)
{
    return cmd != CommandType::POP && cmd != CommandType::PUSH && cmd != CommandType::JE && cmd != CommandType::JNE &&
        cmd != CommandType::JG && cmd != CommandType::JL && cmd != CommandType::JMP && cmd != CommandType::LBL && cmd != CommandType::RET
        && cmd != CommandType::CALL;
}

void Optimizer::movePushPop()
{
    for (int i = 0; i < code.size(); ++i)
    {
        if (code[i]->cmd == CommandType::PUSH)
        {
            string reg = *code[i]->operands[0]->usedRegisters.begin();
            if (code[i]->operands[0]->usedRegisters.empty()) reg = "NAN";
            ++i;
            while (i < code.size() && moveAllowed(code[i]->cmd) &&
                    !code[i]->usedRegisters.count(reg) && !code[i]->usedRegisters.count(registers.at(RSP_REG)))

            {
                swap(code[i - 1], code[i]);
//                cout << i - 1 << " " << i << endl;
                ++i;
            }
        }
    }
    for (int i = code.size() - 1; i >= 0; --i)
    {
        if (code[i]->cmd == CommandType::POP)
        {
            string reg = *(code[i]->operands[0]->usedRegisters.begin());
            if (code[i]->operands[0]->usedRegisters.empty()) reg = "NAN";
            --i;
            while (i > 0 && moveAllowed(code[i]->cmd) &&
                !code[i]->usedRegisters.count(reg) && !code[i]->usedRegisters.count(registers.at(RSP_REG)))
            {
                swap(code[i], code[i + 1]);
//                cout << i + 1 << " " << i << endl;
                --i;
            }
        }
    }
}

void Optimizer::optimize()
{
    bool optRequired = true;
    while (optRequired)
    {
        optRequired = false;
        movePushPop();
        for (int i = 0; i < code.size() - 2; ++i)
        {
            for (int j = 0; j < optimizations.size(); ++j)
            {
                int lines = code.size();
                if (optimizations[j](i))
                {
                    optRequired = true;
                    i = max(0, int(i - lines - code.size()));
                }
            }
        }
    }

    cout << res.str();
}