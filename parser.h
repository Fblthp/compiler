#pragma once

#include "nodes.h"
#include <vector>
#include <memory>
#include <stack>


//TODO exception messages
class ParserException : public std::exception
{
private:
    std::string message;
public:
    ParserException(int line, int pos, std::string messageText)
    {
        std::stringstream ss;
        ss << messageText;
        ss << " line " << line << " pos " << pos;
        message = ss.str();
    }
    ParserException(int line, std::string messageText)
    {
        std::stringstream ss;
        ss << "Parser error line " << line << "\n";
        ss << messageText;
        message = ss.str();
    }
    ParserException(std::string messageText)
    {
        std::stringstream ss;
        ss << messageText;
        message = ss.str();
    }
    const char* what()
    {
        return message.c_str();
    }
};

class Parser
{
private:
    Lexer lexer;
    token_ptr getNext();
    table_ptr currentSymbolTable;
    std::stack<table_ptr> tableStack;

    void require(TokenType type);
    void consume(TokenType type);
    bool isLvalue(node_ptr node);
    void tableAppendWrapper(std::string value, std::string  type);
    void trySetType(TokenType tokenType, node_ptr left, node_ptr right);

    node_ptr parseEmpty();
    node_ptr parseBlock();
    node_ptr parseStmt();
    node_ptr parseGlobal();
    node_ptr parseCondition(bool needed = false);
    node_ptr parseFor();
    node_ptr parseConditionable();
    node_ptr parseDoWhile();
    void parseArrayPart(std::vector<int>& sizes, std::string& type);
    node_ptr parseArray();
    node_ptr parseBrackets();
    node_ptr parseExpr(int priority);
    node_ptr parseFactor();
    node_ptr parseFuncArgs();
    node_ptr parseFuncImplementation();
    node_ptr parseClass();
    node_ptr parseEcho();

public:
    table_ptr globalSymbolTable;
    Parser(char* filename) : lexer(filename)
    {
        globalSymbolTable = table_ptr(new SymbolTable());
        currentSymbolTable = globalSymbolTable;
    };

    void printNode(int lvl, node_ptr node)
    {
        std::cout << std::string(lvl * 4, ' ') << nodeTypesAsString[int(node->type)];
        if (node->getTokenType() != TokenType::UNDEFINED) std::cout << " " << tokens[int(node->getTokenType())];
        if (node->getStrValue() != "") std::cout << " " << node->getStrValue();
        std::cout << std::endl << std::endl;
        for (int i = 0; i < node->children.size(); ++i) printNode(lvl + 1, node->children[i]);
    }

    void printTable()
    {
        for (auto it = globalSymbolTable->table.begin(); it != globalSymbolTable->table.end(); ++it)
        {
            std::cout << it->first << " " << it->second << std::endl;
        }
    }
    node_ptr parse();
};

