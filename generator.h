#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <exception>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <algorithm>
#include <memory>
#include <iomanip>
#include "tokenTypes.h"
#include "symbolTable.h"

enum Reg
{
    RAX_REG,
    RBX_REG,
    RDX_REG,
    RIP_REG,
    RDI_REG,
    RSI_REG,
    RBP_REG,
    RSP_REG,
    AL_REG,
    XMM0_REG,
    XMM1_REG,
};


static std::map<Reg, std::string> registers =
{
    {RAX_REG, "%rax"},
    {RBX_REG, "%rbx"},
    {RDX_REG, "%rdx"},
    {RIP_REG, "%rip"},
    {RDI_REG, "%rdi"},
    {RSI_REG, "%rsi"},
    {RBP_REG, "%rbp"},
    {RSP_REG, "%rsp"},
    {AL_REG, "%al"},
    {XMM0_REG, "%xmm0"},
    {XMM1_REG, "%xmm1"},
};

enum class CommandType
{
    PUSH,
    POP,
    LEAQ,
    MOVQ,
    MOVB,

    RET,
    CQO,

    ADD,
    SUB,
    MUL,
    DIV,
    INC,
    DEC,
    AND,
    OR,
    XOR,
    NOT,
    BIT_AND,
    BIT_OR,
    BIT_XOR,
    BIT_NOT,

    ADDF,
    SUBF,
    DIVF,
    MULF,

    CVTSI2SSL,
    CVTSS2SD,

    CMP,

    JE,
    JNE,
    JL,
    JG,
    JMP,

    CALL,

    LBL
};

struct AsmCommand
{
    CommandType type;
    std::string repr;
    int argsAmount;
    AsmCommand(){};
    AsmCommand(CommandType type, std::string repr, int argsAmount) : type(type), repr(repr), argsAmount(argsAmount){};
};

static std::map<CommandType, AsmCommand> binCommands =
{
    {CommandType::LEAQ, AsmCommand(CommandType::LEAQ, "leaq", 2)},
    {CommandType::MOVQ, AsmCommand(CommandType::MOVQ, "movq", 2)},
    {CommandType::MOVB, AsmCommand(CommandType::MOVB, "movb", 2)},

    {CommandType::ADD, AsmCommand(CommandType::ADD, "addq", 2)},
    {CommandType::SUB, AsmCommand(CommandType::SUB, "subq", 2)},
    {CommandType::MUL, AsmCommand(CommandType::MUL, "imulq", 2)},

    {CommandType::AND, AsmCommand(CommandType::AND, "andq", 2)},
    {CommandType::OR, AsmCommand(CommandType::OR, "orq", 2)},
    {CommandType::XOR, AsmCommand(CommandType::XOR, "xorq", 2)},

//    {CommandType::BIT_AND, AsmCommand(CommandType::BIT_AND, "", 2)},
//    {CommandType::BIT_OR, AsmCommand(CommandType::BIT_OR, "", 2)},
//    {CommandType::BIT_XOR, AsmCommand(CommandType::BIT_XOR, "", 2)},

    {CommandType::ADDF, AsmCommand(CommandType::ADDF, "addsd", 2)},
    {CommandType::SUBF, AsmCommand(CommandType::SUBF, "subsd", 2)},
    {CommandType::MULF, AsmCommand(CommandType::MULF, "mulsd", 2)},
    {CommandType::DIVF, AsmCommand(CommandType::DIVF, "divsd", 2)},

    {CommandType::CVTSI2SSL, AsmCommand(CommandType::CVTSI2SSL, "cvtsi2ssl", 2)},
    {CommandType::CVTSS2SD, AsmCommand(CommandType::CVTSS2SD, "cvtss2sd", 2)},

    {CommandType::CMP, AsmCommand(CommandType::CMP, "cmp", 2)},
};

static std::map<CommandType, AsmCommand> unCommands =
{
    {CommandType::PUSH, AsmCommand(CommandType::PUSH, "pushq", 1)},
    {CommandType::POP, AsmCommand(CommandType::POP, "popq", 1)},

    {CommandType::INC, AsmCommand(CommandType::INC, "incq", 1)},
    {CommandType::DEC, AsmCommand(CommandType::DEC, "decq", 1)},

    {CommandType::BIT_NOT, AsmCommand(CommandType::BIT_NOT, "notq", 1)},
    {CommandType::SUB, AsmCommand(CommandType::SUB, "negq", 1)},
    {CommandType::DIV, AsmCommand(CommandType::DIV, "idivq", 1)},

    {CommandType::JMP, AsmCommand(CommandType::JMP, "jmp", 1)},
    {CommandType::JE, AsmCommand(CommandType::JE, "je", 1)},
    {CommandType::JNE, AsmCommand(CommandType::JNE, "jne", 1)},
    {CommandType::JL, AsmCommand(CommandType::JL, "jl", 1)},
    {CommandType::JG, AsmCommand(CommandType::JG, "jg", 1)},
    {CommandType::CALL, AsmCommand(CommandType::CALL, "call", 1)},

    {CommandType::LBL, AsmCommand(CommandType::LBL, "", 1)},
};

static std::map<CommandType, AsmCommand> singleCommands =
{
    {CommandType::RET, AsmCommand(CommandType::RET, "ret", 0)},
    {CommandType::CQO, AsmCommand(CommandType::CQO, "cqo", 0)},
};

static const std::map<TokenType, CommandType> comparisonCommands =
{
    {TokenType::GREATER_EQUAL,  CommandType::JL},
    {TokenType::LESS_EQUAL, CommandType::JG},
    {TokenType::LESS,  CommandType::JL},
    {TokenType::GREATER, CommandType::JG},
    {TokenType::EQUAL,  CommandType::JE},
    {TokenType::NOT_EQUAL,  CommandType::JNE},
};

static const std::string INT_FMT_LBL = "INT_FMT";
static const std::string FLOAT_FMT_LBL = "FLOAT_FMT";

static const std::string GL_VAR_PREF = "gl_";
static const std::string FUNCTION_PREF = "func_";
static const std::string LBL_PREF = "lbl_";

static std::map<TokenType, CommandType> operatorToCommand
{
    {TokenType::PLUS,  CommandType::ADD},
    {TokenType::MINUS, CommandType::SUB},
    {TokenType::MULT,  CommandType::MUL},
    {TokenType::INC,  CommandType::INC},
    {TokenType::DEC,  CommandType::DEC},
    {TokenType::AND,  CommandType::AND},
    {TokenType::OR,  CommandType::OR},
    {TokenType::XOR,  CommandType::XOR},
    {TokenType::NOT,  CommandType::NOT},
    {TokenType::BIT_NOT,  CommandType::BIT_NOT},
//    {TokenType::BIT_AND,  CommandType::BIT_AND},
//    {TokenType::BIT_OR,  CommandType::BIT_OR},
};

static std::map<TokenType, CommandType> operatorToSSECommand
{
    {TokenType::PLUS,  CommandType::ADDF},
    {TokenType::MINUS,  CommandType::SUBF},
    {TokenType::MULT,  CommandType::MULF},
    {TokenType::DIV,  CommandType::DIVF},
};

static std::map<std::string, std::string> typeToAsm
{
    {buildInTypes[TYPE_INT], ".quad"},
    {buildInTypes[TYPE_FLOAT], ".quad"}, //.long also works
};

static std::map<std::string, int> typeSizes
{
    {buildInTypes[TYPE_INT], 8},
    {buildInTypes[TYPE_FLOAT], 8}
};

struct Operand
{
    virtual std::string getValue() = 0;
    std::set<std::string> usedRegisters;
};
typedef std::shared_ptr<Operand> operand_ptr;

struct ConstantOperand : Operand
{
    std::string value;
    ConstantOperand(std::string value) : value(value){}
    std::string getValue() override
    {
        return value;
    }
};

struct FloatOperand : Operand
{
    std::string value;
    FloatOperand(std::string value) : value(value){}
    std::string getValue() override
    {
        return value;
    }
};

struct Register : Operand
{
    std::string value;
    Register(std::string value) : value(value)
    {
        if (value == registers.at(AL_REG)) usedRegisters.insert(registers.at(RAX_REG));
        else usedRegisters.insert(value);

    }
    std::string getValue() override
    {
        return value;
    }
};

struct VarAdress : Operand
{
    std::string var;
    VarAdress(std::string var) : var(var)
    {
        usedRegisters.insert(registers.at(RIP_REG));
    }
    std::string getValue() override
    {
        return var + "(" + registers.at(RIP_REG) + ")";
    }
};

struct ArgOffset : Operand
{
    std::string offset;
    ArgOffset(int offset)
    {
        this->offset = std::to_string(offset);
        usedRegisters.insert(registers.at(RBP_REG));
    }
    std::string getValue() override
    {
        return offset + "(" + registers.at(RBP_REG) + ")";
    }
};

struct RBPAddr : Operand
{
    RBPAddr()
    {
        usedRegisters.insert(registers.at(RBP_REG));
    }
    std::string getValue() override
    {
        return "(" + registers.at(RBP_REG) + ")";
    }
};
struct OffsetOperand : Operand
{
    std::string reg1;
    std::string reg2;
    OffsetOperand(std::string reg1, std::string reg2) : reg1(reg1), reg2(reg2)
    {
        usedRegisters.insert(reg1);
        usedRegisters.insert(reg2);
        if (usedRegisters.count(registers.at(RAX_REG))) usedRegisters.insert(registers.at(AL_REG));
    }
    std::string getValue() override
    {
        return "(" + reg1 + ", " + reg2 + ", 8)"; //TODO 8
    }
};

static operand_ptr intToOperand(int value)
{
    std::stringstream ss;
    ss << "$" << value;
    return operand_ptr(new ConstantOperand(ss.str()));
}

static operand_ptr floatToOperand(float value)
{
    std::stringstream ss;
    ss << std::fixed << std::setprecision(6) << "$" << value;
    return operand_ptr(new FloatOperand(ss.str()));
}

static operand_ptr stringToNumOperand(std::string value)
{
    return operand_ptr(new ConstantOperand("$" + value));
}

class GeneratorException  : public std::exception
{
private:
    std::string message;
public:
    GeneratorException(std::string messageText)
    {
        std::stringstream ss;
        ss << messageText;
        message = ss.str();
    }
    const char* what()
    {
        return message.c_str();
    }
};

struct Command
{
    CommandType cmd;
    std::vector<operand_ptr> operands;
    std::set<std::string> usedRegisters;
    std::string repr;
    Command(){};
    Command(CommandType cmd) : cmd(cmd)
    {
        repr = singleCommands.at(cmd).repr + "\n";
    };
    Command(CommandType cmd, operand_ptr operand) : cmd(cmd),
        usedRegisters(operand->usedRegisters.begin(), operand->usedRegisters.end())
    {
        operands.push_back(operand);
        if (cmd == CommandType::DIV)
        {
            operands[0]->usedRegisters.insert(registers.at(RAX_REG));
            operands[0]->usedRegisters.insert(registers.at(RDX_REG));
            usedRegisters.insert(registers.at(RAX_REG));
            usedRegisters.insert(registers.at(RDX_REG));
        }
        repr = unCommands.at(cmd).repr + " " + operand->getValue() + "\n";
    };
    Command(CommandType cmd, operand_ptr operand1, operand_ptr operand2) : cmd(cmd),
        usedRegisters(operand1->usedRegisters.begin(), operand1->usedRegisters.end())
    {
        operands.push_back(operand1);
        operands.push_back(operand2);
        repr = binCommands.at(cmd).repr + " " + operand1->getValue() + ", " + operand2->getValue() + "\n";
        for (auto it = operand2->usedRegisters.begin(); it != operand2->usedRegisters.end(); ++it) usedRegisters.insert(*it);
    };
};
typedef std::shared_ptr<Command> command_ptr;

class Generator {
public:
    std::stringstream code;
    std::vector<command_ptr> asmCode;

    std::string lastLbl;

    std::map<std::string, std::string> stringsToLabels;
//    std::map<std::string, std::vector<std::string>> arrayValues;
    std::map<std::string, int> arraySizes;

    std::string generateNextLbl();
    table_ptr curTable;
    table_ptr glTable;

    void init();
    void finish();

    void addLine(CommandType type);
    void addLine(CommandType type, operand_ptr arg);
    void addLine(CommandType type, Reg reg);
    void addLine(CommandType type, std::string arg);
    void addLine(CommandType type, Reg reg1, Reg reg2);
    void addLine(CommandType type, operand_ptr arg, Reg reg);
    void addLine(CommandType type, Reg reg, operand_ptr arg);
    void addLine(CommandType type, operand_ptr arg1, operand_ptr arg2);
    void callPrintf();
    void popReq(Reg reg, std::string type);
    void pushReq(Reg reg);

    operand_ptr getAdr(std::string lbl);
    operand_ptr getArrayAdr(Reg reg1, Reg reg2);

    void addLabel(std::string lbl);

    Generator()
    {
        lastLbl = "zzz";
    }
};

extern Generator generator;