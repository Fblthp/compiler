#pragma once

#include <map>
#include "lexer.h"

enum buildInTypesIdxs : int
{
    TYPE_INT,
    TYPE_ARRAY,
    TYPE_FUNCTION,
    TYPE_BOOL,
    TYPE_FLOAT,
    TYPE_STRING,
    TYPE_OBJECT,
    TYPE_NULL
};

static std::vector<std::string> buildInTypes =
{
    "INT",
    "ARRAY",
    "FUNCTION",
    "BOOL",
    "FLOAT",
    "STRING",
    "OBJECT",
    "NULL"
};

static std::map<TokenType, std::string> tokenToType =
{
    {TokenType::TYPE_INT, buildInTypes[TYPE_INT]},
    {TokenType::TYPE_BOOL, buildInTypes[TYPE_BOOL]},
    {TokenType::TYPE_FLOAT, buildInTypes[TYPE_FLOAT]},
    {TokenType::TYPE_STRING, buildInTypes[TYPE_STRING]},
    {TokenType::OBJECT, buildInTypes[TYPE_OBJECT]},
    {TokenType::UNSET, buildInTypes[TYPE_NULL]},

    {TokenType::INTEGER, buildInTypes[TYPE_INT]},
    {TokenType::DECIMAL, buildInTypes[TYPE_FLOAT]},
    {TokenType::STRING, buildInTypes[TYPE_STRING]},
//    {TokenType::LEFT_BRACKET, buildInTypes[TYPE_ARRAY]},
    {TokenType::ARRAY, buildInTypes[TYPE_ARRAY]},

    {TokenType::ASSIGN_CONCAT, buildInTypes[TYPE_STRING]},
    {TokenType::CONCAT, buildInTypes[TYPE_STRING]},
    {TokenType::ASSIGN_SHL, buildInTypes[TYPE_INT]},
    {TokenType::SHL, buildInTypes[TYPE_INT]},
    {TokenType::ASSIGN_SHR, buildInTypes[TYPE_INT]},
    {TokenType::SHR, buildInTypes[TYPE_INT]},
    {TokenType::ASSIGN_BIT_AND, buildInTypes[TYPE_INT]},
    {TokenType::BIT_AND, buildInTypes[TYPE_INT]},
    {TokenType::ASSIGN_BIT_OR, buildInTypes[TYPE_INT]},
    {TokenType::BIT_OR, buildInTypes[TYPE_INT]},
    {TokenType::ASSIGN_BIT_XOR, buildInTypes[TYPE_INT]},
    {TokenType::BIT_XOR, buildInTypes[TYPE_INT]},
    {TokenType::ASSIGN_MOD, buildInTypes[TYPE_INT]},
    {TokenType::MOD, buildInTypes[TYPE_INT]},

    {TokenType::NOT_EQUAL, buildInTypes[TYPE_BOOL]},
    {TokenType::NOT_IEQUAL, buildInTypes[TYPE_BOOL]},
    {TokenType::EQUAL, buildInTypes[TYPE_BOOL]},
    {TokenType::IEQUAL, buildInTypes[TYPE_BOOL]},
    {TokenType::GREATER, buildInTypes[TYPE_BOOL]},
    {TokenType::GREATER_EQUAL, buildInTypes[TYPE_BOOL]},
    {TokenType::LESS, buildInTypes[TYPE_BOOL]},
    {TokenType::LESS_EQUAL, buildInTypes[TYPE_BOOL]},
    {TokenType::SPACESHIP, buildInTypes[TYPE_BOOL]},
    {TokenType::INSTANCEOF, buildInTypes[TYPE_BOOL]},

    {TokenType::AND, buildInTypes[TYPE_BOOL]},
    {TokenType::OR, buildInTypes[TYPE_BOOL]},
    {TokenType::XOR, buildInTypes[TYPE_BOOL]}
};

static std::set<TokenType> intOperators =
{
    TokenType::PLUS,
    TokenType::MINUS,
    TokenType::MULT,
    TokenType::DIV,
    TokenType::POWER,
    TokenType::ASSIGN_PLUS,
    TokenType::ASSIGN_MINUS,
    TokenType::ASSIGN_MULT,
    TokenType::ASSIGN_DIV,
    TokenType::ASSIGN_POWER,
    TokenType::INC,
    TokenType::DEC
};

static std::map<std::string, std::string> constantsTypes =
{
    {"true", buildInTypes[TYPE_BOOL]},
    {"false", buildInTypes[TYPE_BOOL]},
    {"null", buildInTypes[TYPE_NULL]}
};

//TODO это неправильно
static std::string convertType(std::string type1, std::string type2)
{
    if (type1 == buildInTypes[TYPE_NULL]) return type2;
    if (type2 == buildInTypes[TYPE_NULL]) return type1;
    if (type1 == type2) return type1;
    if (type1 == buildInTypes[TYPE_FLOAT] && type2 == buildInTypes[TYPE_INT]) return buildInTypes[TYPE_FLOAT];
    if (type2 == buildInTypes[TYPE_FLOAT] && type1 == buildInTypes[TYPE_INT]) return buildInTypes[TYPE_FLOAT];
//    return buildInTypes[TYPE_FLOAT];
    return type1;
}

struct Node;
typedef std::shared_ptr<Node> node_ptr;

class SymbolTable;
typedef std::shared_ptr<SymbolTable> table_ptr;

struct ArrayInfo
{
    std::vector<int> sizes;
    std::string type;
    ArrayInfo(){};
    ArrayInfo(std::vector<int> sizes, std::string type) : sizes(sizes), type(type){};
};

class SymbolTable
{
public:
    std::map<std::string, std::string> table;
    std::map<std::string, int> argsOffset;
    std::map<std::string, ArrayInfo> arrays;
    std::set<std::string> globals;
    std::map<std::string, node_ptr> functions;
    std::map<std::string, node_ptr> classes;

    void add(std::string var, std::string type)
    {
        if (table.count(var) && table[var] != type) table[var] = convertType(table[var], type);
        else table[var] = type;
    }
    void addFunction(std::string name, node_ptr value)
    {
        functions[name] = value;
    }
    void addClass(std::string name, node_ptr value)
    {
        if (classes.count(name)) throw;
        classes.emplace(name, value);
    }
    void addGlobal(std::string var)
    {
        globals.insert(var);
    }
};
