#include <iostream>
#include <queue>
#include <stack>
#include <map>
#include <vector>
#include <cmath>
#include <string>
#include <string.h>
#include <sstream>
#include "parser.h"
#include "optimizer.h"

using namespace std;

int main(int argc, char **argv) {
    char* key = "-g";
    char* file = "/Users/aleksandra/Documents/phpCompiler/compiler/test01.in";

    if (argc > 1) file = argv[1];
    if (argc > 2) key = argv[2];

    if (strncmp(key, "-p", 2) == 0 || strncmp(key, "-g", 2) == 0 || strncmp(key, "-o", 2) == 0)
    {
        Parser parser(file);
        node_ptr result;
        try
        {
            result = parser.parse();
        }
        catch (ParserException& e)
        {
            cout << e.what() << endl << endl;
            return 0;
        }
        if (strncmp(key, "-p", 2) == 0)
        {
            parser.printNode(0, result);
            parser.printTable();
        }

        if (strncmp(key, "-g", 2) == 0 || strncmp(key, "-o", 2) == 0)
        {
            try
            {
                generator.glTable = parser.globalSymbolTable;
                generator.curTable = parser.globalSymbolTable;

                generator.init();
                result->generate();
                if (strncmp(key, "-o", 2) == 0)
                {
                    Optimizer optimizer(generator.asmCode);
                    optimizer.movePushPop();
                    optimizer.optimize();
                }
                generator.finish();

                std::fstream out;
                out.open ("test_1.s", std::ifstream::out);

                cout << generator.code.str() << endl;
            }
            catch (GeneratorException& e)
            {
                cout << e.what() << endl << endl;
                return 0;
            }
        }

    }
    if (strncmp(key, "-l", 2) == 0)
    {
        Lexer lexer(file);
        token_ptr token = lexer.getNext();
    }
    return 0;
}