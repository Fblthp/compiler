#pragma once

#include <iomanip>
#include "symbolTable.h"
#include "generator.h"

struct Node;
typedef std::shared_ptr<Node> node_ptr;

enum class NodeType
{
    NONE,
    EMPTY,
    STRING,
    INT,
    FLOAT,
    IDENT,
    VAR,
    BIN_OPERATOR,
    TER_OPERATOR,
    PREFIX_OPERATOR,
    POSTFIX_OPERATOR,
    BLOCK,
    FOR,
    IF,
    WHILE,
    DO_WHILE,
    FUNC_ARGUMENT,
    FUNCTION,
    FUNC_CALL,
    ARGUMENT_LIST,
    CLASS,
    ECHO,
    ARRAY,
    STATIC_ARRAY,
};

static std::vector<std::string> nodeTypesAsString
{
    "NONE",
    "EMPTY",
    "STRING",
    "INT",
    "FLOAT",
    "IDENT",
    "VAR",
    "BIN_OPERATOR",
    "TER_OPERATOR",
    "PREFIX_OPERATOR",
    "POSTFIX_OPERATOR",
    "BLOCK",
    "FOR",
    "IF",
    "WHILE",
    "DO_WHILE",
    "FUNC_ARGUMENT",
    "FUNCTION",
    "FUNC_CALL",
    "ARGUMENT_LIST",
    "CLASS",
    "ECHO",
    "ARRAY",
    "STATIC_ARRAY"
};

//TODO copypasta
static std::string determineType(node_ptr node, table_ptr currentSymbolTable);
static bool isLvalue(node_ptr node);

struct Node
{
    NodeType type;
    table_ptr table;
    bool used;
    std::vector<node_ptr> children;
    Node(NodeType type) : type(type), used(true) {};
    //should be protected
    void addChild(node_ptr child)
    {
        children.push_back(child);
    };
    virtual std::string getStrValue()
    {
        return "";
    }
    virtual TokenType getTokenType()
    {
        return TokenType::UNDEFINED;
    }
    virtual void generate(){}; //should be abstract
    virtual operand_ptr setupVar()
    {
        throw; // not lvalue
    }
};

struct EmptyNode : Node
{
    EmptyNode() : Node(NodeType::EMPTY){};
};

struct SingleValueNode : Node
{
    SingleValueNode(NodeType type) : Node(type){};
};

struct StringValueNode : SingleValueNode
{
    std::string value;
    StringValueNode(std::string value) : SingleValueNode(NodeType::STRING), value(value){};
    std::string getStrValue()
    {
        return value;
    }
    void generate() override
    {
        if (!generator.stringsToLabels.count(value))
            generator.stringsToLabels[value] = generator.generateNextLbl();
        generator.addLine(CommandType::LEAQ, generator.getAdr(generator.stringsToLabels[value]), RAX_REG);
        generator.addLine(CommandType::PUSH, RAX_REG);
    }
};

struct FloatValueNode : SingleValueNode
{
    float value;
    FloatValueNode(float value) : SingleValueNode(NodeType::FLOAT), value(value){};
    std::string getStrValue() override
    {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(6) << value;
        return ss.str();
    }
    void generate() override
    {
        generator.addLine(CommandType::MOVQ, floatToOperand(value), RAX_REG);
        generator.addLine(CommandType::PUSH, RAX_REG);
    }
};

struct IntValueNode : SingleValueNode
{
    int value;
    IntValueNode(int value) : SingleValueNode(NodeType::INT), value(value){};
    std::string getStrValue() override
    {
        std::stringstream ss;
        ss << value;
        return ss.str();
    }
    void generate() override
    {
        generator.addLine(CommandType::PUSH, intToOperand(value));
    }
};

struct IdentNode : Node
{
    std::string name;
    IdentNode(std::string name) : Node(NodeType::IDENT), name(name){};
    std::string getStrValue()
    {
        return name;
    }
};

struct VarNode : Node
{
    std::string name;
    VarNode(std::string name) : Node(NodeType::VAR), name(name){};
    std::string getStrValue() override
    {
        return name;
    }
    void generate() override
    {
        generator.addLine(CommandType::PUSH, setupVar());
    }
    operand_ptr setupVar() override
    {
        if (generator.curTable->argsOffset.count(name)) return operand_ptr(new ArgOffset(generator.curTable->argsOffset.at(name) + 16));
        return generator.getAdr(GL_VAR_PREF + name);
    }
};

struct ArrayNode : Node
{
    node_ptr args;
    ArrayNode(node_ptr args) : args(args), Node(NodeType::ARRAY)
    {
        addChild(args);
    };
};

struct StaticArrayNode : Node
{
    std::vector<int> sizes;
    std::string elementType;
    StaticArrayNode(std::vector<int> sizes, std::string elementType) : sizes(sizes), elementType(elementType), Node(NodeType::STATIC_ARRAY) {};
//    StaticArrayNode(int size) : size(size), Node(NodeType::STATIC_ARRAY) {};
};

struct Field
{
    std::string type;
    int size;
    int offset;
    Field(std::string type, int size, int offset) : type(type), size(size), offset(offset){};
};
struct ClassNode : Node
{
    //TODO change to string
    node_ptr name;
    int totalSize;
//    std::vector<node_ptr> fields;
    std::map<std::string, Field> fields;
    std::map<std::string, ArrayInfo> arrays;
    std::vector<node_ptr> methods;
    ClassNode(node_ptr name) : name(name), Node(NodeType::CLASS){};
    void addField(std::string field, std::string type)
    {
        if (fields.count(field)) throw GeneratorException("class " + name->getStrValue() + " has duplicate fields");
        fields.emplace(field, Field(type, typeSizes.at(type), totalSize));
        totalSize += typeSizes.at(type);
    }
    void addArrayField(std::string field, node_ptr array)
    {
        auto arrayChild = std::dynamic_pointer_cast<StaticArrayNode>(array);
        int size = 0;
        for (int i = 0; i < arrayChild->sizes.size(); ++i) size += arrayChild->sizes[i];
        arrays.emplace(field, ArrayInfo(arrayChild->sizes, arrayChild->elementType));
        fields.emplace(field, Field(arrayChild->getStrValue(), size * typeSizes[arrayChild->elementType], totalSize));
        totalSize += size * typeSizes[arrayChild->elementType];
    }
    void addMethod(node_ptr method)
    {
        methods.push_back(method);
        addChild(method);
    }
    std::string getStrValue() override
    {
        return name->getStrValue();
    }
};

struct OperatorNode : Node
{
    TokenType operation;
    OperatorNode(NodeType type, TokenType operation) : Node(type), operation(operation){};
    TokenType getTokenType() override
    {
        return operation;
    }
};

struct BinOperatorNode : OperatorNode
{
    BinOperatorNode(TokenType operation, node_ptr left, node_ptr right) : OperatorNode(NodeType::BIN_OPERATOR, operation)
    {
        addChild(left);
        addChild(right);
    }

    void generateAssign()
    {
    //TODO!!!
        if (assignOperations.count(operation))
        {
            node_ptr tmp = node_ptr(new BinOperatorNode(assignOperations.at(operation), children[0], children[1]));
            children[1] = tmp;
        }

        std::string name = children[0]->getStrValue();
        if (children[1]->type == NodeType::STATIC_ARRAY)
        {
            auto arrayChild = std::dynamic_pointer_cast<StaticArrayNode>(children[1]);
            int size = 0;
            for (int i = 0; i < arrayChild->sizes.size(); ++i) size += arrayChild->sizes[i];
            if (generator.curTable->arrays.count(name)) throw GeneratorException("Duplicate variable name " + name);
            generator.curTable->arrays.emplace(name, ArrayInfo(arrayChild->sizes, arrayChild->elementType));
            generator.arraySizes[name] = size * typeSizes[arrayChild->elementType];
            return;
        }

        std::string type = determineType(children[1], generator.curTable);
        if (generator.curTable->classes.count(type))
        {
            auto classChild = std::dynamic_pointer_cast<ClassNode>(generator.curTable->classes.at(type));
            int size = classChild->totalSize;
            generator.arraySizes[name] = size;
            return;
        }

        children[1]->generate();
        operand_ptr dst = children[0]->setupVar();

        generator.addLine(CommandType::POP, RAX_REG);
        generator.addLine(CommandType::MOVQ, RAX_REG, dst);
        return;
    }

    void generate() override
    {
        if (assigns.count(operation)) return generateAssign();
        if (operation == TokenType::LEFT_BRACKET || operation == TokenType::OBJECT_OPERATOR)
        {
            generator.addLine(CommandType::MOVQ, setupVar(), RAX_REG);
            if (!used) return;
            generator.addLine(CommandType::PUSH, RAX_REG);
            return;
        }

        for (int i = 0; i < children.size(); ++i) children[i]->generate();

        Reg reg0 = RAX_REG;
        Reg reg1 = RBX_REG;

        const std::map<TokenType, CommandType>* commandList = &operatorToCommand;

        if (convertType(determineType(children[0], generator.glTable),
            determineType(children[1], generator.glTable)) == buildInTypes[TYPE_FLOAT])
        {
            reg0 = XMM0_REG;
            reg1 = XMM1_REG;
            commandList = &operatorToSSECommand;
        }

        if (commandList->count(operation))
        {
            generator.popReq(reg1, determineType(children[1], generator.curTable));
            generator.popReq(reg0, determineType(children[0], generator.curTable));
            if (!used) return;
            generator.addLine((*commandList).at(operation), reg1, reg0);
            generator.pushReq(reg0);
            return;
        }

        generator.addLine(CommandType::POP, RBX_REG);
        generator.addLine(CommandType::POP, RAX_REG);

        switch (operation)
        {
            case TokenType::MOD:
            case TokenType::DIV:
            {
                generator.addLine(CommandType::XOR, RDX_REG, RDX_REG);
                generator.addLine(CommandType::CQO);
                generator.addLine(CommandType::DIV, RBX_REG);
                generator.addLine(CommandType::PUSH, operation == TokenType::MOD ? RDX_REG : RAX_REG);
                break;
            }
            case TokenType::LESS:
            case TokenType::LESS_EQUAL:
            case TokenType::GREATER:
            case TokenType::GREATER_EQUAL:
            case TokenType::EQUAL:
            case TokenType::NOT_EQUAL:
            {
                generator.addLine(CommandType::CMP, RBX_REG, RAX_REG);
                CommandType cmd = comparisonCommands.at(operation);
                bool containsEqual = operation == TokenType::LESS_EQUAL || operation == TokenType::GREATER_EQUAL;

                std::string lbl = generator.generateNextLbl();
                std::string continueLbl = generator.generateNextLbl();

                generator.addLine(cmd, lbl);
                generator.addLine(CommandType::PUSH, intToOperand(containsEqual));
                generator.addLine(CommandType::JMP, continueLbl);

                generator.addLabel(lbl);
                generator.addLine(CommandType::PUSH, intToOperand(!containsEqual));
                generator.addLabel(continueLbl);
                break;
            }
        }
        if (!used) generator.addLine(CommandType::POP, RAX_REG);
    }

    operand_ptr generateField()
    {
        auto node = children[0];
        std::string type = determineType(node, generator.curTable);
        std::string name = node->getStrValue();

        if (!generator.curTable->classes.count(type)) throw GeneratorException("Variable " + name + " has unknown type " + type);
        auto classChild = std::dynamic_pointer_cast<ClassNode>(generator.curTable->classes.at(type));

        std::string fieldName = children[1]->getStrValue();
        if (!classChild->fields.count(fieldName)) throw GeneratorException(fieldName + " is invalid field name");
        auto field = classChild->fields.at(children[1]->getStrValue());

        generator.addLine(CommandType::PUSH, intToOperand(field.offset));

        generator.addLine(CommandType::LEAQ, generator.getAdr(GL_VAR_PREF + name), RBX_REG);
        generator.addLine(CommandType::POP, RDX_REG);
        return generator.getArrayAdr(RBX_REG, RDX_REG);
    }

    operand_ptr setupVar() override
    {
        if (operation == TokenType::OBJECT_OPERATOR) return generateField();
        if (operation != TokenType::LEFT_BRACKET) throw; // expecting lvalue

        auto node = this->children[0];

        while (node->type == NodeType::BIN_OPERATOR && std::dynamic_pointer_cast<BinOperatorNode>(node)->operation == TokenType::LEFT_BRACKET)
            node = node->children[0];

        if (determineType(this->children[1], generator.curTable) != buildInTypes[TYPE_INT]) throw GeneratorException("Invalid array index");
        this->children[1]->generate();

        ArrayInfo info;
        std::string name;
        if (node->type == NodeType::BIN_OPERATOR && std::dynamic_pointer_cast<BinOperatorNode>(node)->operation == TokenType::OBJECT_OPERATOR)
        {
            auto parentNode = node->children[0];
            std::string type = determineType(parentNode, generator.curTable);
            name = parentNode->getStrValue();

            if (!generator.curTable->classes.count(type)) throw GeneratorException("Variable " + name + " has unknown type " + type);
            auto classChild = std::dynamic_pointer_cast<ClassNode>(generator.curTable->classes.at(type));

            std::string fieldName = node->children[1]->getStrValue();
            if (!classChild->fields.count(fieldName)) throw GeneratorException(fieldName + " is invalid field name");
            auto field = classChild->fields.at(node->children[1]->getStrValue());

            info = classChild->arrays.at(fieldName);
            generator.addLine(CommandType::PUSH, intToOperand(field.offset));

            generator.addLine(CommandType::POP, RBX_REG);
            generator.addLine(CommandType::POP, RAX_REG);
            generator.addLine(CommandType::ADD, RBX_REG, RAX_REG);
            generator.addLine(CommandType::PUSH, RAX_REG);
        }
        else
        {
            if (node->type != NodeType::VAR) throw GeneratorException(node->getStrValue() + " is not an array");
            if (!generator.curTable->arrays.count(node->getStrValue())) throw GeneratorException(node->getStrValue() + " is not an array");
            name = node->getStrValue();
            info = generator.curTable->arrays.at(name);
        }

        int counter = info.sizes.size();

        node = this->children[0];

        while (node->type == NodeType::BIN_OPERATOR && std::dynamic_pointer_cast<BinOperatorNode>(node)->operation == TokenType::LEFT_BRACKET)
        {
            if (counter == 1) throw; //invalid array size
            generator.addLine(CommandType::PUSH, intToOperand(info.sizes[--counter]));

            if (determineType(node->children[1], generator.curTable) != buildInTypes[TYPE_INT]) throw GeneratorException("Invalid array index");
            node->children[1]->generate();

            generator.addLine(CommandType::POP, RBX_REG);
            generator.addLine(CommandType::POP, RAX_REG);
            generator.addLine(CommandType::MUL, RBX_REG, RAX_REG);

            generator.addLine(CommandType::POP, RBX_REG);
            generator.addLine(CommandType::ADD, RBX_REG, RAX_REG);
            generator.addLine(CommandType::PUSH, RAX_REG);

            node = node->children[0];
        }
        if (counter != 1) throw GeneratorException("Invalid array size");

        generator.addLine(CommandType::LEAQ, generator.getAdr(GL_VAR_PREF + name), RBX_REG);
        generator.addLine(CommandType::POP, RDX_REG);
        return generator.getArrayAdr(RBX_REG, RDX_REG);
    }
};

struct TerOperatorNode : OperatorNode
{
    TerOperatorNode(TokenType operation, node_ptr left, node_ptr middle, node_ptr right) : OperatorNode(NodeType::TER_OPERATOR, operation)
    {
        addChild(left);
        addChild(middle);
        addChild(right);
    }
};

struct PrefixOperatorNode : OperatorNode
{
    PrefixOperatorNode(TokenType operation, node_ptr child) : OperatorNode(NodeType::PREFIX_OPERATOR, operation)
    {
        addChild(child);
    }
    void generate() override
    {
        if (operation == TokenType::RETURN)
        {
            if (children[0] != nullptr)
            {
                children[0]->generate();
                generator.addLine(CommandType::POP, RAX_REG);
                generator.addLine(CommandType::MOVQ, RAX_REG, operand_ptr(new ArgOffset(generator.curTable->argsOffset.size() * 8 + 16)));
            }
            generator.addLine(CommandType::POP, RBP_REG);
            generator.addLine(CommandType::RET);
            return;
        }
        children[0]->generate();
        if (!operatorToCommand.count(operation)) throw GeneratorException("Unsupported operator " + tokens[(int)operation]);

        generator.addLine(CommandType::POP, RAX_REG);
        generator.addLine(operatorToCommand[operation], RAX_REG);
        generator.addLine(CommandType::PUSH, RAX_REG);

        if (!isLvalue(children[0]) || (operation != TokenType::INC && operation != TokenType::DEC)) return;
        operand_ptr dst = children[0]->setupVar();

        generator.addLine(CommandType::POP, RAX_REG);
        generator.addLine(CommandType::MOVQ, RAX_REG, dst);
        if (used) generator.addLine(CommandType::PUSH, RAX_REG);

    }
};

struct PostfixOperatorNode : OperatorNode
{
    PostfixOperatorNode(TokenType operation, node_ptr child) : OperatorNode(NodeType::POSTFIX_OPERATOR, operation)
    {
        addChild(child);
    }
    void generate() override
    {
        children[0]->generate();
        if (!operatorToCommand.count(operation)) throw GeneratorException("Unsupported operator " + tokens[(int)operation]);

        generator.addLine(CommandType::POP, RAX_REG);
        if (used) generator.addLine(CommandType::PUSH, RAX_REG);
        generator.addLine(operatorToCommand[operation], RAX_REG);

        if (!isLvalue(children[0])) return;

        generator.addLine(CommandType::PUSH, RAX_REG);
        operand_ptr dst = children[0]->setupVar();

        generator.addLine(CommandType::POP, RAX_REG);
        generator.addLine(CommandType::MOVQ, RAX_REG, dst);
    }
};


struct BlockNode : Node
{
    BlockNode() : Node(NodeType::BLOCK){};
    void generate() override
    {
        for (int i = 0; i < children.size(); ++i)
        {
            children[i]->generate();
        }
    }
};

struct ForNode : Node
{
    node_ptr initStatement;
    node_ptr condition;
    node_ptr iterExpression;
    node_ptr block;
    ForNode(node_ptr initStatement, node_ptr condition, node_ptr iterExpression, node_ptr block) : Node(NodeType::FOR),
        initStatement(initStatement), condition(condition), iterExpression(iterExpression), block(block)
    {
        addChild(initStatement);
        addChild(condition);
        addChild(iterExpression);
        addChild(block);
    }
    void generate() override
    {
        initStatement->generate();
        std::string lbl = generator.generateNextLbl();
        std::string finishLbl = generator.generateNextLbl();

        generator.addLabel(lbl);

        condition->generate();
        generator.addLine(CommandType::POP, RAX_REG);
        generator.addLine(CommandType::CMP, intToOperand(0), RAX_REG);
        generator.addLine(CommandType::JE, finishLbl);

        block->generate();
        iterExpression->generate();

        generator.addLine(CommandType::JMP, lbl);
        generator.addLabel(finishLbl);
    }
};

struct IfNode : Node
{
    node_ptr condition;
    node_ptr block;
    node_ptr elseBlock;
    IfNode(node_ptr condition, node_ptr block, node_ptr elseBlock = nullptr) : Node(NodeType::IF), condition(condition),
        block(block), elseBlock(elseBlock)
    {
        addChild(condition);
        addChild(block);
        if (elseBlock != nullptr) addChild(elseBlock);
    }
    void generate() override
    {
        std::string elseLbl = generator.generateNextLbl();
        std::string finishLbl = generator.generateNextLbl();

        condition->generate();
        generator.addLine(CommandType::POP, RAX_REG);
        generator.addLine(CommandType::CMP, intToOperand(0), RAX_REG);
        generator.addLine(CommandType::JE, elseLbl);

        block->generate();
        generator.addLine(CommandType::JMP, finishLbl);

        generator.addLabel(elseLbl);
        if (elseBlock != nullptr) elseBlock->generate();
        generator.addLabel(finishLbl);
    }
};

struct WhileNode : Node
{
    node_ptr condition;
    node_ptr block;
    WhileNode(node_ptr condition, node_ptr block) : Node(NodeType::WHILE), condition(condition), block(block)
    {
        addChild(condition);
        addChild(block);
    }
    void generate() override
    {
        std::string lbl = generator.generateNextLbl();
        std::string finishLbl = generator.generateNextLbl();

        generator.addLabel(lbl);

        condition->generate();
        generator.addLine(CommandType::POP, RAX_REG);
        generator.addLine(CommandType::CMP, intToOperand(0), RAX_REG);
        generator.addLine(CommandType::JE, finishLbl);

        block->generate();
        generator.addLine(CommandType::JMP, lbl);

        generator.addLabel(finishLbl);
    }
};

struct DoWhileNode : Node
{
    node_ptr condition;
    node_ptr block;
    DoWhileNode(node_ptr condition, node_ptr block) : Node(NodeType::DO_WHILE), condition(condition), block(block)
    {
        addChild(condition);
        addChild(block);
    }
    void generate() override
    {
        std::string lbl = generator.generateNextLbl();
        std::string finishLbl = generator.generateNextLbl();

        generator.addLabel(lbl);
        block->generate();

        condition->generate();
        generator.addLine(CommandType::POP, RAX_REG);
        generator.addLine(CommandType::CMP, intToOperand(0), RAX_REG);
        generator.addLine(CommandType::JE, lbl);
    }
};

struct FunctionArgumentNode : Node
{
    node_ptr argType;
    node_ptr argName;
    FunctionArgumentNode(node_ptr argType, node_ptr argName) : Node(NodeType::FUNC_ARGUMENT), argType(argType), argName(argName)
    {
        addChild(argType);
        addChild(argName);
    };
};

struct CalledFunctionArgumentsNode : Node
{
    node_ptr argName;
    CalledFunctionArgumentsNode() : Node(NodeType::ARGUMENT_LIST){};
};

struct FunctionNode : Node
{
    node_ptr name;
    //actually, that's vector of FunctionArguments
    std::vector<node_ptr> arguments;
    node_ptr returnType;
    node_ptr block;
    FunctionNode(node_ptr name, node_ptr returnType, std::vector<node_ptr> args, node_ptr block) : name(name),
        Node(NodeType::FUNCTION), arguments(args), returnType(returnType), block(block)
    {
        for (int i = 0; i < arguments.size(); ++i)
        {
            if (arguments[i]->type != NodeType::FUNC_ARGUMENT) throw;
            addChild(arguments[i]);
        }
        addChild(returnType);
        addChild(block);
    };
    std::string getStrValue() override
    {
        return name->getStrValue();
    }
    void generate() override
    {
        std::string continueLbl = generator.generateNextLbl();
        generator.addLine(CommandType::JMP, continueLbl);

        generator.addLabel(FUNCTION_PREF + name->getStrValue());
        generator.addLine(CommandType::PUSH, RBP_REG);
        generator.addLine(CommandType::MOVQ, RSP_REG, RBP_REG);
        generator.addLine(CommandType::SUB, intToOperand(0), RSP_REG);

        generator.curTable = block->table;
        block->generate();
        generator.curTable = generator.glTable;

        generator.addLine(CommandType::POP, RBP_REG);

        generator.addLine(CommandType::RET);

        generator.addLabel(continueLbl);
    }
};

struct FuncCallNode : Node
{
    node_ptr name;
    node_ptr args;
    FuncCallNode(node_ptr name, node_ptr args) : name(name), args(args), Node(NodeType::FUNC_CALL)
    {
        addChild(args);
    };
    std::string getStrValue() override
    {
        return name->getStrValue();
    }
    void generate() override
    {
        if (!generator.glTable->functions.count(name->getStrValue())) throw GeneratorException("Unknown function " + name->getStrValue());
        auto func = std::dynamic_pointer_cast<FunctionNode>(generator.glTable->functions.at(name->getStrValue()));
        if (func->arguments.size() != args->children.size()) throw GeneratorException("Wrong amount of arguments in " + name->getStrValue() + "call," +
            std::to_string(func->arguments.size()) + " expected, " + std::to_string(args->children.size()) + " given");

        if (func->returnType != nullptr) generator.addLine(CommandType::SUB, intToOperand(8), RSP_REG);
        for (int i = args->children.size() - 1; i >= 0; --i)
        {
            std::string type = determineType(args->children[i], generator.curTable);
            if (type == buildInTypes[TYPE_INT]) generator.addLine(CommandType::PUSH, stringToNumOperand(args->children[i]->getStrValue()));
            if (type == buildInTypes[TYPE_FLOAT])
            {
                generator.addLine(CommandType::MOVQ, stringToNumOperand(args->children[i]->getStrValue()), RAX_REG);
                generator.addLine(CommandType::PUSH, RAX_REG);
            }
        }
        generator.addLine(CommandType::CALL, FUNCTION_PREF + name->getStrValue());
        generator.addLine(CommandType::ADD, intToOperand(8 * args->children.size()), RSP_REG);
        if (!used && func->returnType != nullptr) generator.addLine(CommandType::ADD, intToOperand(8), RSP_REG);

    }
};

struct EchoNode : Node
{
    EchoNode() : Node(NodeType::ECHO){};
    void generate() override
    {
        for (int i = 0; i < children.size(); ++i)
        {
            children[i]->generate();
            if (children[i]->type == NodeType::STRING)
            {
                generator.addLine(CommandType::POP, RDI_REG);
            }
            else
            {
                std::string fmt = INT_FMT_LBL;
                Reg outputReg = RSI_REG;
                std::string type = determineType(children[i], generator.curTable);
                if (type == buildInTypes[TYPE_FLOAT])
                {
                    fmt = FLOAT_FMT_LBL;
                    outputReg = XMM0_REG;
                }
                generator.addLine(CommandType::LEAQ, generator.getAdr(fmt), RDI_REG);
                generator.popReq(outputReg, type);
            }
            generator.callPrintf();

        }
    }
};

static bool isLvalue(node_ptr node)
{
    return node->type == NodeType::VAR ||
        (node->type == NodeType::BIN_OPERATOR && (
            (std::dynamic_pointer_cast<BinOperatorNode>(node))->operation == TokenType::LEFT_BRACKET ||
            (std::dynamic_pointer_cast<BinOperatorNode>(node))->operation == TokenType::OBJECT_OPERATOR
        ));
}

static std::string determineType(node_ptr node, table_ptr currentSymbolTable)
{
    std::string type = buildInTypes[TYPE_NULL];
    if (node->type == NodeType::INT) return buildInTypes[TYPE_INT];
    if (node->type == NodeType::FLOAT) return buildInTypes[TYPE_FLOAT];
    if (node->type == NodeType::STRING) return buildInTypes[TYPE_STRING];

    if (node->type == NodeType::VAR)
    {
        if (currentSymbolTable->arrays.count(node->getStrValue())) return currentSymbolTable->arrays.at(node->getStrValue()).type;
        if (currentSymbolTable->table.count(node->getStrValue())) return currentSymbolTable->table.at(node->getStrValue());
    }

    if (node->type == NodeType::BIN_OPERATOR && (std::dynamic_pointer_cast<BinOperatorNode>(node))->operation == TokenType::OBJECT_OPERATOR)
    {
        std::string type = determineType(node->children[0], generator.curTable);
        std::string name = node->children[0]->getStrValue();

        if (!generator.curTable->classes.count(type)) throw GeneratorException(type + " is invalid class name");
        auto classChild = std::dynamic_pointer_cast<ClassNode>(generator.curTable->classes.at(type));

        auto field = classChild->fields.at(node->children[1]->getStrValue());
        return field.type;
    }

    if (node->type == NodeType::PREFIX_OPERATOR && (std::dynamic_pointer_cast<PrefixOperatorNode>(node))->operation == TokenType::NEW)
    {
        if (node->children[0]->type != NodeType::FUNC_CALL) throw GeneratorException("Invalid class");
        return node->children[0]->getStrValue();
    }

    if (node->type == NodeType ::IDENT && constantsTypes.count(node->getStrValue()))
        return constantsTypes[node->getStrValue()];

    TokenType tokenType = node->getTokenType();
    if (tokenType != TokenType::UNDEFINED)
    {
        if (tokenToType.count(tokenType)) return tokenToType[tokenType];
    }
    for (int i = (node->type == NodeType::TER_OPERATOR); i < node->children.size(); ++i)
    {
        std::string newType = determineType(node->children[i], currentSymbolTable);
        type = convertType(type, newType);
    }
    if (intOperators.count(tokenType))
    {
        if (type != buildInTypes[TYPE_FLOAT]) type = buildInTypes[TYPE_INT];
        return type;
    }
    return type;
}