//subq $0, %rsp
//-->
//pushq 16(%rbp)
//popq %rbx
//-->
//movq 16(%rbp), %rbx

//pushq 24(%rbp)
//popq %rbx
//-->
//movq 24(%rbp), %rbx

//pushq %rax
//popq %rbx
//-->
//movq %rax, %rbx

//pushq %rax
//popq %rax
//-->
//pushq 16(%rbp)
//popq %rax
//-->
//movq 16(%rbp), %rax

//pushq 24(%rbp)
//popq %rax
//-->
//movq 24(%rbp), %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
pushq %rbp
jmp lbl_aaaa
 func_foo:
pushq %rbp
movq %rsp, %rbp
movq 16(%rbp), %rax
movq 16(%rbp), %rbx
imulq %rbx, %rax
pushq %rax
movq 24(%rbp), %rax
movq 24(%rbp), %rbx
imulq %rbx, %rax
movq %rax, %rbx
popq %rax
subq %rbx, %rax
movq %rax, 32(%rbp)
popq %rbp
ret
popq %rbp
ret
 lbl_aaaa:
subq $8, %rsp
pushq $10
pushq $12
call func_foo
addq $16, %rsp
popq %rsi
leaq INT_FMT(%rip), %rdi
call _printf
popq %rbp
ret
.data

