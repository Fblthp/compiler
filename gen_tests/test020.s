//pushq %rax
//popq %rax
//-->
//pushq %rax
//popq %rax
//-->
//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq lbl_aaaa(%rip), %rax
//movq %rax, %rdi
//-->
//leaq lbl_aaaa(%rip), %rdi

//pushq %rax
//popq %rax
//-->
//pushq %rax
//popq %rax
//-->
.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $1.200000, %rax
pushq %rbp
pushq %rax
movq $3.100000, %rax
movq %rax, %xmm1
movb $1, %al
popq %rax
movq %rax, %xmm0
movb $1, %al
mulsd %xmm1, %xmm0
movq %xmm0, %rax
leaq FLOAT_FMT(%rip), %rdi
movq %rax, %xmm0
movb $1, %al
call _printf
leaq lbl_aaaa(%rip), %rdi
call _printf
movq $9.300000, %rax
pushq %rax
movq $1.400000, %rax
movq %rax, %xmm1
movb $1, %al
popq %rax
movq %rax, %xmm0
movb $1, %al
divsd %xmm1, %xmm0
movq %xmm0, %rax
leaq FLOAT_FMT(%rip), %rdi
movq %rax, %xmm0
movb $1, %al
call _printf
popq %rbp
ret
lbl_aaaa:
.string " \0"
.data

