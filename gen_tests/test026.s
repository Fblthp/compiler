//pushq $10
//popq %rax
//-->
//movq $10, %rax

//movq $10, %rax
//movq %rax, (%rbp)
//-->
//movq $10, (%rbp)

//pushq %rax
//popq %rax
//-->
//pushq %rax
//popq %rax
//-->
.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $1.500000, %rax
pushq %rbp
movq $10, (%rbp)
cvtsi2ssl (%rbp), %xmm1
cvtss2sd %xmm1, %xmm1
movq %rax, %xmm0
movb $1, %al
subsd %xmm1, %xmm0
movq %xmm0, %rax
leaq FLOAT_FMT(%rip), %rdi
movq %rax, %xmm0
movb $1, %al
call _printf
popq %rbp
ret
.data

