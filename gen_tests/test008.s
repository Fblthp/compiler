//pushq $0
//popq %rbx
//-->
//movq $0, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq lbl_aaaa(%rip), %rax
//movq %rax, %rdi
//-->
//leaq lbl_aaaa(%rip), %rdi

//pushq $0
//popq %rbx
//-->
//movq $0, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq lbl_aaaa(%rip), %rax
//movq %rax, %rdi
//-->
//leaq lbl_aaaa(%rip), %rdi

//pushq $1
//popq %rbx
//-->
//movq $1, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $1
//popq %rax
//-->
//movq $1, %rax

//pushq $0
//popq %rax
//-->
//movq $0, %rax

//pushq $1
//popq %rax
//-->
//movq $1, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $1, %rax
movq $0, %rbx
andq %rbx, %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
leaq lbl_aaaa(%rip), %rdi
call _printf
movq $0, %rbx
movq $0, %rax
andq %rbx, %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
leaq lbl_aaaa(%rip), %rdi
call _printf
movq $1, %rbx
movq $1, %rax
andq %rbx, %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
popq %rbp
ret
lbl_aaaa:
.string " \0"
.data

