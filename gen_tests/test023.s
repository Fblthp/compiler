//pushq $2
//popq %rax
//-->
//movq $2, %rax

//movq $2, %rax
//movq %rax, gl_$a(%rip)
//-->
//movq $2, gl_$a(%rip)

//pushq %rax
//popq %rax
//-->
//pushq gl_$b(%rip)
//popq %rax
//-->
//movq gl_$b(%rip), %rax

//movq gl_$b(%rip), %rax
//movq %rax, %xmm1
//-->
//movq gl_$b(%rip), %xmm1

//pushq %rax
//popq %rax
//-->
//movq %xmm0, %rax
//movq %rax, gl_$c(%rip)
//-->
//movq %xmm0, gl_$c(%rip)

//pushq gl_$c(%rip)
//popq %rax
//-->
//movq gl_$c(%rip), %rax

//movq %xmm0, gl_$c(%rip)
//movq gl_$c(%rip), %rax
//-->
//movq %xmm0, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2, gl_$a(%rip)
movq $1.200000, %rax
movq %rax, gl_$b(%rip)
pushq %rbp
pushq gl_$a(%rip)
movq gl_$b(%rip), %xmm1
movb $1, %al
popq %rax
movq %rax, (%rbp)
cvtsi2ssl (%rbp), %xmm0
cvtss2sd %xmm0, %xmm0
mulsd %xmm1, %xmm0
movq %xmm0, %rax
leaq FLOAT_FMT(%rip), %rdi
movq %rax, %xmm0
movb $1, %al
call _printf
popq %rbp
ret
.data
gl_$a: .quad 0 
gl_$b: .quad 0 
gl_$c: .quad 0 

