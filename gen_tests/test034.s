//pushq $0
//popq %rax
//-->
//movq $0, %rax

//movq $0, %rax
//movq %rax, gl_$i(%rip)
//-->
//movq $0, gl_$i(%rip)

//pushq $5
//popq %rbx
//-->
//movq $5, %rbx

//pushq gl_$i(%rip)
//popq %rsi
//-->
//movq gl_$i(%rip), %rsi

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq aaae(%rip), %rax
//movq %rax, %rdi
//-->
//leaq aaae(%rip), %rdi

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $0, gl_$i(%rip)
pushq %rbp
 lbl_aaaa:
movq $5, %rbx
movq gl_$i(%rip), %rax
cmp %rbx, %rax
jl aaac
pushq $0
jmp aaad
 aaac:
pushq $1
 aaad:
popq %rax
cmp $0, %rax
je aaab
movq gl_$i(%rip), %rsi
leaq INT_FMT(%rip), %rdi
call _printf
leaq aaae(%rip), %rdi
call _printf
movq gl_$i(%rip), %rax
incq %rax
movq %rax, gl_$i(%rip)
jmp lbl_aaaa
 aaab:
popq %rbp
ret
aaae:
.string " \0"
.data
gl_$i: .quad 0 

