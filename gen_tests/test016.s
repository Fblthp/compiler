//pushq $4
//popq %rbx
//-->
//movq $4, %rbx

//pushq %rax
//popq %rbx
//-->
//movq %rax, %rbx

//pushq %rax
//popq %rax
//-->
//pushq gl_$a(%rip)
//popq %rsi
//-->
//movq gl_$a(%rip), %rsi

//movq %rax, gl_$a(%rip)
//movq gl_$a(%rip), %rsi
//-->
//movq %rax, %rsi

//pushq $3
//popq %rax
//-->
//movq $3, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
pushq %rbp
pushq $2
movq $4, %rbx
movq $3, %rax
imulq %rbx, %rax
movq %rax, %rbx
popq %rax
addq %rbx, %rax
movq %rax, %rsi
leaq INT_FMT(%rip), %rdi
call _printf
popq %rbp
ret
.data
gl_$a: .quad 0 

