//pushq $2
//popq %rbx
//-->
//movq $2, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq lbl_aaaa(%rip), %rax
//movq %rax, %rdi
//-->
//leaq lbl_aaaa(%rip), %rdi

//pushq $3
//popq %rbx
//-->
//movq $3, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $8
//popq %rax
//-->
//movq $8, %rax

//pushq $7
//popq %rax
//-->
//movq $7, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $8, %rax
movq $2, %rbx
xorq %rdx, %rdx
cqo
idivq %rbx
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
leaq lbl_aaaa(%rip), %rdi
call _printf
movq $3, %rbx
movq $7, %rax
xorq %rdx, %rdx
cqo
idivq %rbx
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
popq %rbp
ret
lbl_aaaa:
.string " \0"
.data

