//pushq $2
//popq %rax
//-->
//movq $2, %rax

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//movq $2, %rax
//movq %rax, %rsi
//-->
//movq $2, %rsi

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2, %rsi
incq %rax
leaq INT_FMT(%rip), %rdi
pushq %rbp
call _printf
popq %rbp
ret
.data

