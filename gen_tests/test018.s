//pushq %rax
//popq %rax
//-->
.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2.500000, %rax
leaq FLOAT_FMT(%rip), %rdi
movq %rax, %xmm0
movb $1, %al
pushq %rbp
call _printf
popq %rbp
ret
.data

