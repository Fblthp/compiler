//pushq $3
//popq %rax
//-->
//movq $3, %rax

//movq $3, %rax
//movq %rax, (%rbp)
//-->
//movq $3, (%rbp)

//pushq $10
//popq %rbx
//-->
//movq $10, %rbx

//pushq %rdx
//popq %rax
//-->
//movq %rdx, %rax

//movq %rdx, %rax
//movq %rax, (%rbp)
//-->
//movq %rdx, (%rbp)

//pushq %rax
//popq %rax
//-->
//pushq %rax
//popq %rax
//-->
//pushq $13
//popq %rax
//-->
//movq $13, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2.100000, %rax
pushq %rbp
movq $3, (%rbp)
cvtsi2ssl (%rbp), %xmm1
cvtss2sd %xmm1, %xmm1
movq %rax, %xmm0
movb $1, %al
mulsd %xmm1, %xmm0
movq %xmm0, %rax
pushq %rax
movq $13, %rax
movq $10, %rbx
xorq %rdx, %rdx
cqo
idivq %rbx
popq %rax
movq %rdx, (%rbp)
cvtsi2ssl (%rbp), %xmm1
cvtss2sd %xmm1, %xmm1
movq %rax, %xmm0
movb $1, %al
addsd %xmm1, %xmm0
movq %xmm0, %rax
leaq FLOAT_FMT(%rip), %rdi
movq %rax, %xmm0
movb $1, %al
call _printf
popq %rbp
ret
.data

