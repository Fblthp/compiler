//pushq $0
//popq %rax
//-->
//movq $0, %rax

//movq $0, %rax
//movq %rax, gl_$i(%rip)
//-->
//movq $0, gl_$i(%rip)

//pushq $100
//popq %rbx
//-->
//movq $100, %rbx

//pushq gl_$i(%rip)
//popq %rbx
//-->
//movq gl_$i(%rip), %rbx

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq $0
//popq %rax
//-->
//movq $0, %rax

//movq $0, %rax
//movq %rax, gl_$l(%rip)
//-->
//movq $0, gl_$l(%rip)

//pushq $100
//popq %rax
//-->
//movq $100, %rax

//movq $100, %rax
//movq %rax, gl_$r(%rip)
//-->
//movq $100, gl_$r(%rip)

//pushq $200
//popq %rax
//-->
//movq $200, %rax

//movq $200, %rax
//movq %rax, gl_$val(%rip)
//-->
//movq $200, gl_$val(%rip)

//pushq gl_$l(%rip)
//popq %rbx
//-->
//movq gl_$l(%rip), %rbx

//pushq $1
//popq %rbx
//-->
//movq $1, %rbx

//pushq gl_$r(%rip)
//popq %rbx
//-->
//movq gl_$r(%rip), %rbx

//pushq $2
//popq %rbx
//-->
//movq $2, %rbx

//pushq %rax
//popq %rax
//-->
//pushq gl_$m(%rip)
//popq %rdx
//-->
//movq gl_$m(%rip), %rdx

//pushq %rax
//popq %rbx
//-->
//movq %rax, %rbx

//movq (%rbx, %rdx, 8), %rax
//movq %rax, %rbx
//-->
//movq (%rbx, %rdx, 8), %rbx

//pushq gl_$m(%rip)
//popq %rax
//-->
//movq gl_$m(%rip), %rax

//pushq gl_$m(%rip)
//popq %rax
//-->
//movq gl_$m(%rip), %rax

//pushq gl_$l(%rip)
//popq %rdx
//-->
//movq gl_$l(%rip), %rdx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq gl_$r(%rip)
//popq %rax
//-->
//movq gl_$r(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq gl_$l(%rip)
//popq %rax
//-->
//movq gl_$l(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq gl_$val(%rip)
//popq %rax
//-->
//movq gl_$val(%rip), %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $0, gl_$i(%rip)
pushq %rbp
 lbl_aaaa:
movq $100, %rbx
movq gl_$i(%rip), %rax
cmp %rbx, %rax
jl aaac
pushq $0
jmp aaad
 aaac:
pushq $1
 aaad:
popq %rax
cmp $0, %rax
je aaab
movq gl_$i(%rip), %rax
movq gl_$i(%rip), %rbx
imulq %rbx, %rax
pushq %rax
pushq gl_$i(%rip)
leaq gl_$a(%rip), %rbx
popq %rdx
popq %rax
movq %rax, (%rbx, %rdx, 8)
movq gl_$i(%rip), %rax
incq %rax
movq %rax, gl_$i(%rip)
jmp lbl_aaaa
 aaab:
movq $0, gl_$l(%rip)
movq $100, gl_$r(%rip)
movq $200, gl_$val(%rip)
 aaae:
movq gl_$r(%rip), %rax
movq gl_$l(%rip), %rbx
subq %rbx, %rax
movq $1, %rbx
cmp %rbx, %rax
jg aaag
pushq $0
jmp aaah
 aaag:
pushq $1
 aaah:
popq %rax
cmp $0, %rax
je aaaf
movq gl_$l(%rip), %rax
movq gl_$r(%rip), %rbx
addq %rbx, %rax
movq $2, %rbx
xorq %rdx, %rdx
cqo
idivq %rbx
movq %rax, gl_$m(%rip)
movq gl_$val(%rip), %rax
movq gl_$m(%rip), %rdx
leaq gl_$a(%rip), %rbx
movq (%rbx, %rdx, 8), %rbx
cmp %rbx, %rax
jl aaak
pushq $0
jmp aaal
 aaak:
pushq $1
 aaal:
popq %rax
cmp $0, %rax
je aaai
movq gl_$m(%rip), %rax
movq %rax, gl_$r(%rip)
jmp aaaj
 aaai:
movq gl_$m(%rip), %rax
movq %rax, gl_$l(%rip)
 aaaj:
jmp aaae
 aaaf:
movq gl_$l(%rip), %rdx
leaq gl_$a(%rip), %rbx
movq (%rbx, %rdx, 8), %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
popq %rbp
ret
.data
gl_$i: .quad 0 
gl_$l: .quad 0 
gl_$m: .quad 0 
gl_$r: .quad 0 
gl_$val: .quad 0 
gl_$a: .zero 800

