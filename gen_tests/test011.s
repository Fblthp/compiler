//pushq $13
//popq %rax
//-->
//movq $13, %rax

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $13, %rax
notq %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
popq %rbp
ret
.data

