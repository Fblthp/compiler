//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $1
//popq %rax
//-->
//movq $1, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $1, %rax
leaq gl_$a(%rip), %rbx
movq $0, %rdx
movq %rax, (%rbx, %rdx, 8)
leaq gl_$a(%rip), %rbx
movq $0, %rdx
movq (%rbx, %rdx, 8), %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
popq %rbp
ret
.data
gl_$a: .zero 24

