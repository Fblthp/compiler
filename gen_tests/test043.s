//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq $3
//popq %rbx
//-->
//movq $3, %rbx

//pushq $8
//popq %rdx
//-->
//movq $8, %rdx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $13
//popq %rax
//-->
//movq $13, %rax

//pushq %rax
//popq %rax
//-->
//pushq $8
//popq %rdx
//-->
//movq $8, %rdx

//pushq %rax
//popq %rax
//-->
.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $13, %rax
leaq gl_$d(%rip), %rbx
movq $0, %rdx
movq %rax, (%rbx, %rdx, 8)
leaq gl_$d(%rip), %rbx
movq $0, %rdx
movq (%rbx, %rdx, 8), %rax
movq $3, %rbx
subq %rbx, %rax
leaq gl_$d(%rip), %rbx
movq $8, %rdx
movq %rax, (%rbx, %rdx, 8)
leaq gl_$d(%rip), %rbx
movq $8, %rdx
movq (%rbx, %rdx, 8), %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
popq %rbp
ret
.data
gl_$d: .zero 16

