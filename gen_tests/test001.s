//pushq $2
//popq %rbx
//-->
//movq $2, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $2
//popq %rax
//-->
//movq $2, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2, %rax
movq $2, %rbx
addq %rbx, %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
popq %rbp
ret
.data

