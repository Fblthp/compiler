//pushq $1
//popq %rdx
//-->
//movq $1, %rdx

//pushq $1
//popq %rdx
//-->
//movq $1, %rdx

//pushq %rax
//popq %rax
//-->
//pushq %rax
//popq %rax
//-->
.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $1.500000, %rax
leaq gl_$a(%rip), %rbx
movq $1, %rdx
movq %rax, (%rbx, %rdx, 8)
leaq gl_$a(%rip), %rbx
movq $1, %rdx
movq (%rbx, %rdx, 8), %rax
leaq FLOAT_FMT(%rip), %rdi
movq %rax, %xmm0
movb $1, %al
pushq %rbp
call _printf
popq %rbp
ret
.data
gl_$a: .zero 40

