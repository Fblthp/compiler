//pushq %rax
//popq %rax
//-->
//pushq gl_$a(%rip)
//popq %rax
//-->
//movq gl_$a(%rip), %rax

//movq %rax, gl_$a(%rip)
//movq gl_$a(%rip), %rax
//-->
//movq %rax, %rax

//movq $2.800000, %rax
//movq %rax, %rax
//-->
//movq $2.800000, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2.800000, %rax
leaq FLOAT_FMT(%rip), %rdi
movq %rax, %xmm0
movb $1, %al
pushq %rbp
call _printf
popq %rbp
ret
.data
gl_$a: .quad 0 

