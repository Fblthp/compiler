//subq $0, %rsp
//-->
//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq aaab(%rip), %rax
//movq %rax, %rdi
//-->
//leaq aaab(%rip), %rdi

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
pushq %rbp
jmp lbl_aaaa
 func_foo:
pushq %rbp
movq %rsp, %rbp
leaq aaab(%rip), %rdi
call _printf
popq %rbp
ret
 lbl_aaaa:
call func_foo
addq $0, %rsp
popq %rbp
ret
aaab:
.string "foo\0"
.data

