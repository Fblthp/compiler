//pushq $12
//popq %rax
//-->
//movq $12, %rax

//movq $12, %rax
//movq %rax, gl_$a(%rip)
//-->
//movq $12, gl_$a(%rip)

//pushq $3
//popq %rax
//-->
//movq $3, %rax

//movq $3, %rax
//movq %rax, gl_$b(%rip)
//-->
//movq $3, gl_$b(%rip)

//pushq gl_$b(%rip)
//popq %rbx
//-->
//movq gl_$b(%rip), %rbx

//pushq %rax
//popq %rax
//-->
//pushq gl_$c(%rip)
//popq %rsi
//-->
//movq gl_$c(%rip), %rsi

//movq %rax, gl_$c(%rip)
//movq gl_$c(%rip), %rsi
//-->
//movq %rax, %rsi

//pushq gl_$a(%rip)
//popq %rax
//-->
//movq gl_$a(%rip), %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $12, gl_$a(%rip)
movq $3, gl_$b(%rip)
movq gl_$a(%rip), %rax
movq gl_$b(%rip), %rbx
xorq %rdx, %rdx
cqo
idivq %rbx
movq %rax, %rsi
leaq INT_FMT(%rip), %rdi
pushq %rbp
call _printf
popq %rbp
ret
.data
gl_$a: .quad 0 
gl_$b: .quad 0 
gl_$c: .quad 0 

