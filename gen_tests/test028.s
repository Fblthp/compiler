//pushq $1
//popq %rdx
//-->
//movq $1, %rdx

//pushq $1
//popq %rdx
//-->
//movq $1, %rdx

//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq %rax
//popq %rbx
//-->
//movq %rax, %rbx

//movq (%rbx, %rdx, 8), %rax
//movq %rax, %rbx
//-->
//movq (%rbx, %rdx, 8), %rbx

//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq lbl_aaaa(%rip), %rax
//movq %rax, %rdi
//-->
//leaq lbl_aaaa(%rip), %rdi

//pushq $1
//popq %rdx
//-->
//movq $1, %rdx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq lbl_aaaa(%rip), %rax
//movq %rax, %rdi
//-->
//leaq lbl_aaaa(%rip), %rdi

//pushq $2
//popq %rdx
//-->
//movq $2, %rdx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $4
//popq %rax
//-->
//movq $4, %rax

//pushq %rax
//popq %rax
//-->
//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq $1
//popq %rax
//-->
//movq $1, %rax

//pushq $2
//popq %rdx
//-->
//movq $2, %rdx

//pushq %rax
//popq %rax
//-->
.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $4, %rax
leaq gl_$a(%rip), %rbx
movq $1, %rdx
movq %rax, (%rbx, %rdx, 8)
movq $1, %rax
leaq gl_$a(%rip), %rbx
movq $0, %rdx
movq %rax, (%rbx, %rdx, 8)
leaq gl_$a(%rip), %rbx
movq $1, %rdx
movq (%rbx, %rdx, 8), %rax
movq $0, %rdx
leaq gl_$a(%rip), %rbx
movq (%rbx, %rdx, 8), %rbx
addq %rbx, %rax
leaq gl_$a(%rip), %rbx
movq $2, %rdx
movq %rax, (%rbx, %rdx, 8)
leaq gl_$a(%rip), %rbx
movq $0, %rdx
movq (%rbx, %rdx, 8), %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
leaq lbl_aaaa(%rip), %rdi
call _printf
leaq gl_$a(%rip), %rbx
movq $1, %rdx
movq (%rbx, %rdx, 8), %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
leaq lbl_aaaa(%rip), %rdi
call _printf
leaq gl_$a(%rip), %rbx
movq $2, %rdx
movq (%rbx, %rdx, 8), %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
popq %rbp
ret
lbl_aaaa:
.string " \0"
.data
gl_$a: .zero 24

