//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq $2
//popq %rax
//-->
//movq $2, %rax

//movq $2, %rax
//movq %rax, gl_$i(%rip)
//-->
//movq $2, gl_$i(%rip)

//pushq $20
//popq %rbx
//-->
//movq $20, %rbx

//pushq $1
//popq %rbx
//-->
//movq $1, %rbx

//pushq %rax
//popq %rdx
//-->
//movq %rax, %rdx

//pushq $2
//popq %rbx
//-->
//movq $2, %rbx

//pushq %rax
//popq %rdx
//-->
//movq %rax, %rdx

//pushq %rax
//popq %rbx
//-->
//movq %rax, %rbx

//movq (%rbx, %rdx, 8), %rax
//movq %rax, %rbx
//-->
//movq (%rbx, %rdx, 8), %rbx

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq $0
//popq %rax
//-->
//movq $0, %rax

//movq $0, %rax
//movq %rax, gl_$i(%rip)
//-->
//movq $0, gl_$i(%rip)

//pushq $20
//popq %rbx
//-->
//movq $20, %rbx

//pushq gl_$i(%rip)
//popq %rdx
//-->
//movq gl_$i(%rip), %rdx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq aaai(%rip), %rax
//movq %rax, %rdi
//-->
//leaq aaai(%rip), %rdi

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq $1
//popq %rax
//-->
//movq $1, %rax

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq $1
//popq %rdx
//-->
//movq $1, %rdx

//pushq $1
//popq %rax
//-->
//movq $1, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $1, %rax
leaq gl_$a(%rip), %rbx
movq $0, %rdx
movq %rax, (%rbx, %rdx, 8)
movq $1, %rax
leaq gl_$a(%rip), %rbx
movq $1, %rdx
movq %rax, (%rbx, %rdx, 8)
movq $2, gl_$i(%rip)
pushq %rbp
 lbl_aaaa:
movq $20, %rbx
movq gl_$i(%rip), %rax
cmp %rbx, %rax
jl aaac
pushq $0
jmp aaad
 aaac:
pushq $1
 aaad:
popq %rax
cmp $0, %rax
je aaab
movq $1, %rbx
movq gl_$i(%rip), %rax
subq %rbx, %rax
leaq gl_$a(%rip), %rbx
movq %rax, %rdx
movq (%rbx, %rdx, 8), %rax
pushq %rax
movq gl_$i(%rip), %rax
movq $2, %rbx
subq %rbx, %rax
leaq gl_$a(%rip), %rbx
movq %rax, %rdx
popq %rax
movq (%rbx, %rdx, 8), %rbx
addq %rbx, %rax
pushq %rax
pushq gl_$i(%rip)
leaq gl_$a(%rip), %rbx
popq %rdx
popq %rax
movq %rax, (%rbx, %rdx, 8)
movq gl_$i(%rip), %rax
incq %rax
movq %rax, gl_$i(%rip)
jmp lbl_aaaa
 aaab:
movq $0, gl_$i(%rip)
 aaae:
movq $20, %rbx
movq gl_$i(%rip), %rax
cmp %rbx, %rax
jl aaag
pushq $0
jmp aaah
 aaag:
pushq $1
 aaah:
popq %rax
cmp $0, %rax
je aaaf
movq gl_$i(%rip), %rdx
leaq gl_$a(%rip), %rbx
movq (%rbx, %rdx, 8), %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
leaq aaai(%rip), %rdi
call _printf
movq gl_$i(%rip), %rax
incq %rax
movq %rax, gl_$i(%rip)
jmp aaae
 aaaf:
popq %rbp
ret
aaai:
.string " \0"
.data
gl_$i: .quad 0 
gl_$a: .zero 160

