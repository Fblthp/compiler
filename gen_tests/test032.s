//pushq $2
//popq %rbx
//-->
//movq $2, %rbx

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq aaac(%rip), %rax
//movq %rax, %rdi
//-->
//leaq aaac(%rip), %rdi

//pushq $1
//popq %rbx
//-->
//movq $1, %rbx

//pushq $1
//popq %rax
//-->
//movq $1, %rax

//pushq $1
//popq %rax
//-->
//movq $1, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $1, %rax
movq $2, %rbx
cmp %rbx, %rax
pushq %rbp
jne lbl_aaaa
pushq $0
jmp aaab
 lbl_aaaa:
pushq $1
 aaab:
popq %rsi
leaq INT_FMT(%rip), %rdi
call _printf
leaq aaac(%rip), %rdi
call _printf
movq $1, %rbx
movq $1, %rax
cmp %rbx, %rax
jne aaad
pushq $0
jmp aaae
 aaad:
pushq $1
 aaae:
popq %rsi
leaq INT_FMT(%rip), %rdi
call _printf
popq %rbp
ret
aaac:
.string " \0"
.data

