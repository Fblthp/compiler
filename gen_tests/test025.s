//pushq $200
//popq %rbx
//-->
//movq $200, %rbx

//pushq $5
//popq %rbx
//-->
//movq $5, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $100
//popq %rax
//-->
//movq $100, %rax

//pushq %rax
//popq %rax
//-->
.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $100, %rax
movq $200, %rbx
subq %rbx, %rax
movq $5, %rbx
xorq %rdx, %rdx
cqo
idivq %rbx
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
popq %rbp
ret
.data

