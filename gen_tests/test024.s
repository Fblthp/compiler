//pushq $2
//popq %rax
//-->
//movq $2, %rax

//pushq $3
//popq %rbx
//-->
//movq $3, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq %rax
//popq %rax
//-->
.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2, %rax
negq %rax
movq $3, %rbx
subq %rbx, %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
pushq %rbp
call _printf
popq %rbp
ret
.data

