//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq $0
//popq %rax
//-->
//movq $0, %rax

//movq $0, %rax
//movq %rax, gl_$i(%rip)
//-->
//movq $0, gl_$i(%rip)

//pushq $4
//popq %rbx
//-->
//movq $4, %rbx

//pushq $0
//popq %rax
//-->
//movq $0, %rax

//movq $0, %rax
//movq %rax, gl_$j(%rip)
//-->
//movq $0, gl_$j(%rip)

//pushq $4
//popq %rbx
//-->
//movq $4, %rbx

//pushq $0
//popq %rdx
//-->
//movq $0, %rdx

//pushq gl_$i(%rip)
//popq %rbx
//-->
//movq gl_$i(%rip), %rbx

//pushq gl_$j(%rip)
//popq %rbx
//-->
//movq gl_$j(%rip), %rbx

//pushq $8
//popq %rbx
//-->
//movq $8, %rbx

//pushq gl_$i(%rip)
//popq %rbx
//-->
//movq gl_$i(%rip), %rbx

//pushq %rax
//popq %rdx
//-->
//movq %rax, %rdx

//pushq gl_$j(%rip)
//popq %rax
//-->
//movq gl_$j(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq $0
//popq %rax
//-->
//movq $0, %rax

//movq $0, %rax
//movq %rax, gl_$i(%rip)
//-->
//movq $0, gl_$i(%rip)

//pushq $4
//popq %rbx
//-->
//movq $4, %rbx

//pushq $0
//popq %rax
//-->
//movq $0, %rax

//movq $0, %rax
//movq %rax, gl_$j(%rip)
//-->
//movq $0, gl_$j(%rip)

//pushq $4
//popq %rbx
//-->
//movq $4, %rbx

//pushq $8
//popq %rbx
//-->
//movq $8, %rbx

//pushq gl_$i(%rip)
//popq %rbx
//-->
//movq gl_$i(%rip), %rbx

//pushq %rax
//popq %rdx
//-->
//movq %rax, %rdx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq aaaq(%rip), %rax
//movq %rax, %rdi
//-->
//leaq aaaq(%rip), %rdi

//pushq gl_$j(%rip)
//popq %rax
//-->
//movq gl_$j(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq aaar(%rip), %rax
//movq %rax, %rdi
//-->
//leaq aaar(%rip), %rdi

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq $7
//popq %rax
//-->
//movq $7, %rax

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq gl_$j(%rip)
//popq %rax
//-->
//movq gl_$j(%rip), %rax

//pushq %rax
//popq %rax
//-->
//pushq %rax
//popq %rax
//-->
//pushq gl_$j(%rip)
//popq %rax
//-->
//movq gl_$j(%rip), %rax

//pushq $4
//popq %rax
//-->
//movq $4, %rax

//pushq gl_$i(%rip)
//popq %rax
//-->
//movq gl_$i(%rip), %rax

//pushq gl_$j(%rip)
//popq %rax
//-->
//movq gl_$j(%rip), %rax

//pushq gl_$j(%rip)
//popq %rax
//-->
//movq gl_$j(%rip), %rax

//pushq $4
//popq %rax
//-->
//movq $4, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $7, %rax
leaq gl_$d(%rip), %rbx
movq $0, %rdx
movq %rax, (%rbx, %rdx, 8)
movq $0, gl_$i(%rip)
pushq %rbp
 lbl_aaaa:
movq $4, %rbx
movq gl_$i(%rip), %rax
cmp %rbx, %rax
jl aaac
pushq $0
jmp aaad
 aaac:
pushq $1
 aaad:
popq %rax
cmp $0, %rax
je aaab
movq $0, gl_$j(%rip)
 aaae:
movq $4, %rbx
movq gl_$j(%rip), %rax
cmp %rbx, %rax
jl aaag
pushq $0
jmp aaah
 aaag:
pushq $1
 aaah:
popq %rax
cmp $0, %rax
je aaaf
leaq gl_$d(%rip), %rbx
movq $0, %rdx
movq (%rbx, %rdx, 8), %rax
movq gl_$i(%rip), %rbx
imulq %rbx, %rax
movq gl_$j(%rip), %rbx
subq %rbx, %rax
pushq %rax
movq gl_$j(%rip), %rax
movq $8, %rbx
addq %rbx, %rax
pushq %rax
movq $4, %rax
movq gl_$i(%rip), %rbx
imulq %rbx, %rax
popq %rbx
addq %rbx, %rax
leaq gl_$d(%rip), %rbx
movq %rax, %rdx
popq %rax
movq %rax, (%rbx, %rdx, 8)
movq gl_$j(%rip), %rax
incq %rax
movq %rax, gl_$j(%rip)
jmp aaae
 aaaf:
movq gl_$i(%rip), %rax
incq %rax
movq %rax, gl_$i(%rip)
jmp lbl_aaaa
 aaab:
movq $0, gl_$i(%rip)
 aaai:
movq $4, %rbx
movq gl_$i(%rip), %rax
cmp %rbx, %rax
jl aaak
pushq $0
jmp aaal
 aaak:
pushq $1
 aaal:
popq %rax
cmp $0, %rax
je aaaj
movq $0, gl_$j(%rip)
 aaam:
movq $4, %rbx
movq gl_$j(%rip), %rax
cmp %rbx, %rax
jl aaao
pushq $0
jmp aaap
 aaao:
pushq $1
 aaap:
popq %rax
cmp $0, %rax
je aaan
movq $8, %rbx
movq gl_$j(%rip), %rax
addq %rbx, %rax
pushq %rax
movq $4, %rax
movq gl_$i(%rip), %rbx
imulq %rbx, %rax
popq %rbx
addq %rbx, %rax
leaq gl_$d(%rip), %rbx
movq %rax, %rdx
movq (%rbx, %rdx, 8), %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
leaq aaaq(%rip), %rdi
call _printf
movq gl_$j(%rip), %rax
incq %rax
movq %rax, gl_$j(%rip)
jmp aaam
 aaan:
leaq aaar(%rip), %rdi
call _printf
movq gl_$i(%rip), %rax
incq %rax
movq %rax, gl_$i(%rip)
jmp aaai
 aaaj:
popq %rbp
ret
aaaq:
.string " \0"
aaar:
.string "\n\0"
.data
gl_$i: .quad 0 
gl_$j: .quad 0 
gl_$d: .zero 72

