//pushq $4
//popq %rbx
//-->
//movq $4, %rbx

//pushq %rax
//popq %rbx
//-->
//movq %rax, %rbx

//pushq $6
//popq %rbx
//-->
//movq $6, %rbx

//pushq %rax
//popq %rbx
//-->
//movq %rax, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $3
//popq %rax
//-->
//movq $3, %rax

//pushq $13
//popq %rax
//-->
//movq $13, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
pushq %rbp
pushq $2
movq $4, %rbx
movq $3, %rax
imulq %rbx, %rax
movq %rax, %rbx
popq %rax
addq %rbx, %rax
pushq %rax
movq $13, %rax
movq $6, %rbx
xorq %rdx, %rdx
cqo
idivq %rbx
movq %rax, %rbx
popq %rax
subq %rbx, %rax
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
popq %rbp
ret
.data

