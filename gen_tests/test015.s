//pushq $2
//popq %rax
//-->
//movq $2, %rax

//movq $2, %rax
//movq %rax, gl_$a(%rip)
//-->
//movq $2, gl_$a(%rip)

//pushq gl_$a(%rip)
//popq %rsi
//-->
//movq gl_$a(%rip), %rsi

//movq $2, gl_$a(%rip)
//movq gl_$a(%rip), %rsi
//-->
//movq $2, %rsi

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2, %rsi
leaq INT_FMT(%rip), %rdi
pushq %rbp
call _printf
popq %rbp
ret
.data
gl_$a: .quad 0 

