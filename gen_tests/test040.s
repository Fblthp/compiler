//pushq $2
//popq %rax
//-->
//movq $2, %rax

//movq $2, %rax
//movq %rax, gl_$a(%rip)
//-->
//movq $2, gl_$a(%rip)

//subq $0, %rsp
//-->
//pushq $7
//popq %rbx
//-->
//movq $7, %rbx

//pushq %rax
//popq %rax
//-->
//addq $0, %rsp
//-->
//pushq gl_$a(%rip)
//popq %rsi
//-->
//movq gl_$a(%rip), %rsi

//pushq gl_$a(%rip)
//popq %rax
//-->
//movq gl_$a(%rip), %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2, gl_$a(%rip)
pushq %rbp
jmp lbl_aaaa
 func_foo:
pushq %rbp
movq %rsp, %rbp
popq %rbp
movq $7, %rbx
movq gl_$a(%rip), %rax
addq %rbx, %rax
movq %rax, gl_$a(%rip)
ret
 lbl_aaaa:
call func_foo
movq gl_$a(%rip), %rsi
leaq INT_FMT(%rip), %rdi
call _printf
popq %rbp
ret
.data
gl_$a: .quad 0 

