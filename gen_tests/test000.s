//pushq %rax
//popq %rdi
//-->
//movq %rax, %rdi

//leaq lbl_aaaa(%rip), %rax
//movq %rax, %rdi
//-->
//leaq lbl_aaaa(%rip), %rdi

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
leaq lbl_aaaa(%rip), %rdi
pushq %rbp
call _printf
popq %rbp
ret
lbl_aaaa:
.string "ok\0"
.data

