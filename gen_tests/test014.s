//pushq $3
//popq %rbx
//-->
//movq $3, %rbx

//pushq $1
//popq %rbx
//-->
//movq $1, %rbx

//pushq %rax
//popq %rbx
//-->
//movq %rax, %rbx

//pushq %rax
//popq %rsi
//-->
//movq %rax, %rsi

//pushq $2
//popq %rax
//-->
//movq $2, %rax

//pushq $2
//popq %rax
//-->
//movq $2, %rax

.cstring
.text
.globl _main
INT_FMT:
.ascii "%d\0"
FLOAT_FMT:
.ascii "%.5f\0"
_main:
movq $2, %rax
movq $3, %rbx
imulq %rbx, %rax
pushq %rbp
pushq %rax
movq $2, %rax
movq $1, %rbx
addq %rbx, %rax
movq %rax, %rbx
popq %rax
xorq %rdx, %rdx
cqo
idivq %rbx
leaq INT_FMT(%rip), %rdi
movq %rax, %rsi
call _printf
popq %rbp
ret
.data

