import subprocess
import os

def runTest(test, key):
	fo = open(test + '.out')
	ans = (subprocess.check_output(['/Users/aleksandra/Library/Caches/CLion2016.1/cmake/generated/compiler-1714b183/1714b183/Debug/compiler', test + '.in', key]))
	expectedAns = fo.read()
	if (ans == expectedAns): 
		print("OK")
	else: 
		print test
		for j in range(min(len(ans), len(expectedAns))):
			if (ans[j] != expectedAns[j]):
				print("    " + str(j) + " " + ans[j] + " " + expectedAns[j])
	fo.close()

def runGenTest(test, key):
	ans = (subprocess.check_output(['/Users/aleksandra/Library/Caches/CLion2016.1/cmake/generated/compiler-1714b183/1714b183/Debug/compiler', test + '.in', key]))
	fo = open( test + '.s', 'w')
	fo.write(ans)
	fo.close()
	os.system('gcc ' + test + '.s' + ' -o ' + test + '.o')
	res = os.popen(test + '.o').read()

	expOutput = open(test + '.out').read()
	if (expOutput == res):
		print 'OK'
	else:
		print test

# print("Lexer tests:")
# for i in range(0, 32):
	# runTest('tests/test' + '0' * (len(str(100)) - len(str(i))) + str(i), '-l')

# print("\nParser tests:")
# for i in range(0, 37):
# 			runTest('/Users/aleksandra/Documents/phpCompiler/compiler/parser_tests/test' + '0' * (len(str(100)) - len(str(i))) + str(i), '-p')

# print("\nMore parser tests:")
# for i in range(0, 16):
# 	runTest('/Users/aleksandra/Documents/phpCompiler/compiler/more_parser_tests/test' + '0' * (len(str(100)) - len(str(i))) + str(i), '-p')

print("\nGenerator tests:")
for i in range(0, 45):
	runGenTest('/Users/aleksandra/Documents/phpCompiler/compiler/gen_tests/test' + '0' * (len(str(100)) - len(str(i))) + str(i), '-g')

print("\nOptimizer tests:")
for i in range(0, 45):
	runGenTest('/Users/aleksandra/Documents/phpCompiler/compiler/gen_tests/test' + '0' * (len(str(100)) - len(str(i))) + str(i), '-o')

